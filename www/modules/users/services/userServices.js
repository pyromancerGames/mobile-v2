(function () {

    'use strict';

    angular.module('classbuzz.user')

        .factory('userServices', function ($http, $rootScope, authenticationServices, apiServices, _) {

            var services = {

                /**
                 * Makes API Call to fetch te full public user profile
                 * @param [userId] defaults to logged user
                 * @returns {*}
                 */
                getProfile: function(userId) {
                    // Get username
                    if (typeof userId == "undefined" || userId == null) userId = authenticationServices.userProfile.id;
                    var endpoint = '/users/' + userId + '/public';
                    return apiServices.GET(endpoint);
                },

                /**
                 * Returns whether user has role or not
                 * @param role
                 * @param [user] - defaults to logged user
                 * @returns {boolean}
                 */
                hasRole: function(roleName, user) {
                    if (typeof user == "undefined" || user == null) user = authenticationServices.userProfile;

                    //If the user dont have a roles array then obviously dont have the role.
                    if (!user.roles) return false;
                    var names = user.roles.map(function (r) {
                        return r.name
                    });
                    return names.indexOf(roleName) != -1;
                },

                /**
                 * Make API Call to persist avatar changes.
                 * @param profileData
                 * @returns {*}
                 */
                updateAvatarURL: function (profileData){

                    var endpoint = '/users/avatar/update';
                    return apiServices.POST(endpoint, profileData);
                },

                /**
                 * Make API Call to persist profile changes.
                 * @param profileData
                 * @returns {*}
                 */
                updateProfile: function (profileData){
                    var endpoint = '/users';
                    return apiServices.PUT(endpoint, profileData);

                },

                /**
                 * Make API Call to update user password
                 * @param newPassword
                 * @returns {*}
                 */
                updatePassword: function (newPassword){
                    var data = {password: newPassword};
                    var endpoint = '/users/password';
                    return apiServices.PUT(endpoint, data);
                },

                updateUserPass: function(userId, newPassword) {
                    var data = {"password": newPassword};
                    var endpoint = '/users/' + userId + '/password/reset';
                    return apiServices.POST(endpoint, data);
                },


                /**
                 * Make API Call to persist profile changes.
                 * @param profileData
                 * @returns {*}
                 */
                addHouse: function (houseId, userId){

                    if (typeof userId == "undefined" || userId == null) userId = authenticationServices.userProfile.id;
                    var endpoint = '/users/'+ userId +'/house';
                    var data = {
                        id: houseId
                    }
                    return apiServices.PUT(endpoint, data);

                },

                /**
                 * Get user username.
                 * @param [userId] defaults to logged user.
                 * @returns {*}
                 */
                getUsername: function(userId){
                    if (typeof userId == "undefined" || userId == null) userId = authenticationServices.userProfile.id;
                    var endpoint = '/users/' + userId + '/username';
                    return apiServices.GET(endpoint);
                },

                /**
                 * Get users from a customer/branch with selected roles.
                 * @param [userId] defaults to logged user.
                 * @returns {*}
                 */
                getWithRole: function(params){
                    if (typeof params.customer == "undefined" || params.customer == null)
                        params.customer = authenticationServices.userProfile.customer;

                    if (typeof params.branch == "undefined" || params.branch == null)
                        params.branch = authenticationServices.userProfile.branch;

                    var endpoint = '/users/lazy';
                    return apiServices.GET(endpoint, params);
                },



                /**
                 * Adds achievement to user
                 * @param userId
                 * @param achievementId
                 */
                addActivity: function(userId, activityId) {

                    var _data = {
                        _id: activityId
                    };

                    var endpoint = '/users/' + userId + '/activities';
                    return apiServices.POST(endpoint, _data);

                },

                /**
                 * Get user active quests
                 * @param [userId] - defaults to logged user
                 * @returns {*}
                 */
                getActiveQuests: function(userId) {
                    if (typeof userId == "undefined" || userId == null) userId = authenticationServices.userProfile.id;
                    var endpoint = '/users/' + userId + '/quests';
                    return apiServices.GET(endpoint);
                },


                /**
                 * Get user completed quests
                 * @param [userId] - defaults to logged user
                 * @returns {*}
                 */
                getCompletedQuests: function(userId) {
                    if (typeof userId == "undefined" || userId == null) userId = authenticationServices.userProfile.id;
                    var endpoint = '/users/' + userId + '/quests';
                    return apiServices.GET(endpoint, {completed:true});
                },

                /**
                 * Get user active special events
                 * @param [userId] - defaults to logged user
                 * @returns {*}
                 */
                getActiveSpecialEvents: function(userId) {
                    if (typeof userId == "undefined" || userId == null) userId = authenticationServices.userProfile.id;
                    var endpoint = '/users/' + userId + '/specialEvents';
                    return apiServices.GET(endpoint);
                },

                /**
                 * Get user active special events
                 * @param [userId] - defaults to logged user
                 * @returns {*}
                 */
                getCompletedSpecialEvents: function(userId) {
                    if (typeof userId == "undefined" || userId == null) userId = authenticationServices.userProfile.id;
                    var endpoint = '/users/' + userId + '/specialEvents/completed';
                    return apiServices.GET(endpoint);
                },

                /**
                 * Get user recent activities
                 * @param userId
                 * @param [limit] - Defaults to -1 (all)
                 * @returns {*}
                 */
                getRecentActivities: function(userId, limit) {
                    if (typeof limit == "undefined" || limit == null) limit = -1;

                    var _data = {
                        limit: limit
                    };

                    var _querystring = _.toQueryString(_data);

                    var endpoint = '/users/' + userId + '/activities' + _querystring;
                    return apiServices.GET(endpoint);
                },

                /**
                 * Get user achievements (all time)
                 * @param [userId] - defaults to logged user
                 * @returns {*}
                 */
                getAchievements: function (userId) {
                    if (typeof userId == "undefined" || userId == null) userId = authenticationServices.userProfile.id;
                    var endpoint = '/users/' + userId + '/achievements';
                    return apiServices.GET(endpoint);
                },

                getUser: function (userId) {
                    if (typeof userId == "undefined" || userId == null) userId = authenticationServices.userProfile.id;
                    var endpoint = '/users/' + userId;
                    return apiServices.GET(endpoint);
                },

                /**
                 * Get user avatar URI
                 * @param [userId] - defaults to logged user
                 * @returns {*}
                 */
                getAvatarURI: function (userId) {
                    if (typeof userId == "undefined" || userId == null) userId = authenticationServices.userProfile.id;
                    var endpoint = '/users/' + userId + '/avatar';
                    return apiServices.GET(endpoint);
                },

                /**
                 * Get user character
                 * @param id
                 * @returns {*}
                 */
                getUserCharacter: function(id){
                    var endpoint = '/users/' + id + '/character';
                    return apiServices.GET(endpoint);
                },

                /**
                 * Get user children
                 * @param id
                 * @returns {*}
                 */
                getChildren: function(userId){
                    if (typeof userId == "undefined" || userId == null) userId = authenticationServices.userProfile.id;
                    var endpoint = '/users/' + userId + '/children';
                    return apiServices.GET(endpoint);
                },

                /**
                 * Get user children
                 * @param id
                 * @returns {*}
                 */
                getUserChildren: function(id, params){
                    var endpoint = '/users/' + id + '/children';
                    return apiServices.GET(endpoint, params);
                },

                getChildrenParents: function(id, params){
                    var endpoint = '/public/users/' + id + '/parents';
                    return apiServices.GET(endpoint, params);
                },

                /**
                 * Updates user character
                 * @param char
                 * @param userDoc
                 * @returns {*}
                 */
                updateChar: function(char, id) {
                    var endpoint = '/users/' + id + '/character';
                    return apiServices.PUT(endpoint, char);
                },

                /**
                 * Buy trait (updates character)
                 * @param traitId
                 * @param userDoc
                 * @returns {*}
                 */
                buyTrait: function(traitId, userId) {

                    var _data = {
                        traitId: traitId
                    };

                    var endpoint = '/users/' + userId + '/trait/buy'
                    return apiServices.POST(endpoint, _data);
                },

                /**
                 * Updates user avatar base64 img
                 * @param avatarURL
                 * @returns {*}
                 */
                updateUserAvatar: function(avatarURL) {
                    var _data = {_id: authenticationServices.userProfile.id, avatarURL: avatarURL};
                    var endpoint = '/users'
                    return apiServices.PUT(endpoint, _data);
                },


                /**
                 * Updates user settings
                 * @param settings
                 * @returns {*}
                 */
                updateSettings: function(settings) {
                    var userId = authenticationServices.userProfile.id
                    var endpoint = '/users/' + userId + '/settings'
                    return apiServices.PUT(endpoint, settings);
                },


                 /**
                 * Get user settings
                 * @param settings
                 * @returns {*}
                 */
                getSettings: function() {
                    var userId = authenticationServices.userProfile.id
                    var endpoint = '/users/' + userId + '/settings'
                    return apiServices.GET(endpoint);
                }

            };

            return services;
        });

})();

