angular.module('classbuzz.leaderboards')

    .controller('LeaderboardsController', function ($http, $rootScope, $scope, $socketio, $moment,
                                                    $ionicModal, $ionicListDelegate, authenticationServices,
                                                    errorServices, houseServices, leaderboardsServices) {

        $scope.houses = {};

        $scope.rankRunners = [];
        $scope.rankPodium = [];
        $scope.loggedUser = authenticationServices.userProfile;

        $scope.sliderIdx = 1; //0=Houses; 1=Students

        getHouseLeaderBoard();
        getStudentsLeaderBoard();

        $scope.setSlider = function(idx) {
            $scope.sliderIdx = idx;
        }

        $scope.doRefresh = function () {
            getHouseLeaderBoard();
        }

        function getStudentsLeaderBoard(){
            leaderboardsServices.getStudentLeaderboard()
                .then(function (students) {

                    $scope.rankPodium = students.data.slice(0,3);

                    $scope.rankRunners = students.data.slice(3,students.data.length + 1);

                    console.log($scope.rankRunners);

                    $scope.$broadcast('scroll.refreshComplete'); // Stop the ion-refresher from spinning
                })
                .catch(function (err) {
                    errorServices.logError(err, "trying to get house leaderboard");
                });
        }

        function getHouseLeaderBoard(){
            houseServices.getHouses()
                .then(function (houses) {
                    $scope.houses = houses.data;
                    $scope.$broadcast('scroll.refreshComplete'); // Stop the ion-refresher from spinning
                })
                .catch(function (err) {
                    errorServices.logError(err, "trying to get house leaderboard");
                });
        }


    })
