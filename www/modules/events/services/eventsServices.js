(function () {
    angular.module('classbuzz.events')
        .factory('eventServices', function ($rootScope, $localStorage, $location, _, apiServices,
                                            authenticationServices) {
            var services = {

                /**
                 * Get events for a customer
                 * @param [customerId] - defaults to logged user customer
                 * @returns {*}
                 */
                getEvents: function (customerId) {

                    if (typeof customerId == "undefined" || customerId == null) customerId = authenticationServices.userProfile.customer;

                    var endpoint = '/events/customer/' + customerId;
                    return apiServices.GET(endpoint);
                },


                /**
                 * Get events for a customer, lazy loading
                 * @returns {*}
                 */
                getLazyEvents: function (from) {

                    var customerId = authenticationServices.userProfile.customer;
                    var _data = {

                        fromIndex: from,
                        limit: 6,
                        customer: customerId
                    };

                    var endpoint = '/events/lazy';
                    return apiServices.POST(endpoint, _data);

                }
            };

            return services;
        });

})();




