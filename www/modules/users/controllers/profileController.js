angular.module('classbuzz.user')

    .controller('ProfileController', function ($http, $scope, $rootScope, $stateParams, $translate, $moment, ionicDatePicker,
                                               $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $cordovaDatePicker,
                                               $ionicModal, $ionicLoading, $ionicActionSheet, $timeout, $interval,
                                               authenticationServices, errorServices, userServices,
                                               cbUtils, awsServices, messagesServices, cameraServices,
                                               $cordovaToast, rankServices, _) {

        fetchFullUserProfile();
        fetchRanks();

        $scope.saving = false;
        $scope.userProfile = {};
        $scope.editMode = false;
        $scope.ranks = [];
        $scope.lowestRank = {};
        $scope.groupCount = 0;
        $scope.studentCount = 0;

        $scope.tooltips = {
            overallRank: function() {

                var tooltip = {
                    title: "Your rank",
                    text: "Level all skills up to reach a new overall rank!",
                    btnText: "Got it"
                };

                $rootScope.$broadcast('cb.showtooltip', tooltip);
            }
        }


        $scope.uploadProgress = {
            current: 0,
            total: 0
        };

        $scope.rankByOrdinal = rankByOrdinal;

        $scope.uploading = false;
        $scope.editMode = false;
        $scope.avatarURI = "img/avatar-placeholder.png";
        $scope.newMessage = {};

        // Create the send mail modal
        $ionicModal.fromTemplateUrl('modules/users/views/sendParentMessageModal.html', {scope: $scope})
            .then(function (modal) {
                $scope.sendMessageModal = modal;
            });
        //======================================================================================

        // Reset password
        $scope.changePassword = changePassword;
        $scope.closeModal = closeModal;
        $scope.doChangePassword = doChangePassword;
        $scope.passwordsDontMatch = false;
        $scope.passwordData = {
            password1: "",
            password2: ""
        };

        function changePassword () {
            $ionicModal.fromTemplateUrl('modules/settings/views/changePassModal.html', { scope: $scope })
                .then(function (modal) {
                    $scope.modal = modal;
                    $scope.modal.show();
                });
        };

        function closeModal() {
            $scope.modal.hide();
            $scope.modal.remove();
            $rootScope.$broadcast('cb.hidetooltip');
        };

        function doChangePassword(){

            if ($scope.passwordData.password1 != $scope.passwordData.password2) {
                $scope.passwordsDontMatch = true;
                return;
            }

            $scope.passwordsDontMatch = false;

            $translate("messages.updating_password").then(function(fetchingText){
                fetchingText = '<p>'+fetchingText+'</p><ion-spinner></ion-spinner>';
                $ionicLoading.show({template:fetchingText});
            });

            userServices.updateUserPass($stateParams.userId, $scope.passwordData.password2)
                .then( function(result){
                    $ionicLoading.hide();
                    $scope.closeModal();
                    $translate("messages.saved").then(function(fetchingText){
                        $ionicLoading.show({template:fetchingText, duration:1000});
                    });
                    $scope.passwordData = { password1: "", password2: "" };
                })
                .catch(function(err){
                    $translate("messages.embarrassing_error").then(function(fetchingText){
                        fetchingText = '<p>'+fetchingText+'</p><ion-spinner></ion-spinner>';
                        $ionicLoading.show({template:fetchingText, duration:3000});
                    });
                })
        }

        $scope.isMyProfile = function(){
            return $stateParams.userId == authenticationServices.userProfile.id;
        }

        $scope.hasRole = hasRole;

        /**
         * Wrap call to pass custom userProfile.
         * @param roleName
         * @returns {*|boolean}
         */
        function hasRole (roleName) {
            return userServices.hasRole(roleName, $scope.userProfile);
        };

        /**
         * Exits edit mode saving the changes
         */
        $scope.saveProfile = function () {
            $scope.saving = true;

            var editableProfile = angular.copy($scope.userProfile);
            delete editableProfile.avatarURL;
            delete editableProfile.credentials;

            userServices.updateProfile(editableProfile)
                .then(function(result){

                    $scope.saving = false;

                    var tooltip = {
                        text: "Profile saved!",
                        btnText: "Cool",
                        showBtn: false,
                        duration: 1500
                    };

                    $rootScope.$broadcast('cb.showtooltip', tooltip);

                })
        };


        // $scope.openDatePicker = function() {
        //     //Will fail in browser. To be tested in emulator or actual device.
        //     var options = {
        //         date: new Date(),
        //         mode: 'date', // or 'time'
        //         allowOldDates: true,
        //         allowFutureDates: false,
        //         doneButtonLabel: 'DONE',
        //         doneButtonColor: '#202145',
        //         cancelButtonLabel: 'CANCEL',
        //         cancelButtonColor: '#FF0066'
        //     };

        //     $cordovaDatePicker.show(options).then(function(date){
        //         $scope.userProfile.birthday = $moment(date);
        //         $scope.userProfile.humanBirthday = $moment(date).format("MMMM Do YYYY");
        //     });

        // };

        $scope.openDatePicker = function() {

            var initData = {

                callback: function (val) {  //Mandatory
                    $scope.userProfile.birthday = $moment(val);
                    $scope.userProfile.humanBirthday = $moment(val).format("MMMM Do YYYY");
                },

                inputDate: new Date(),      //Optional
                mondayFirst: true,          //Optional
                closeOnSelect: false,       //Optional
                templateType: 'popup'       //Optional
            };

            ionicDatePicker.openDatePicker(initData);
        };



        function fetchRanks(){
            rankServices.getRanks()
                .then(function(result){
                    $scope.ranks = result.data;
                })
        }

        function rankByOrdinal(ordinal) {
            return _.find($scope.ranks, function(r) {
                return r.ordinality == ordinal;
            })
        }

        function computeLowestRank() {
            var lowestSkill = _.minBy($scope.userProfile.student.character.skills, function(s) {
                return s.rankInfo.rank;
            })

            $scope.lowestRank = rankByOrdinal(lowestSkill.rankInfo.rank);
        }


        function computeStats() {
            $scope.groupCount = $scope.userProfile.groupsOwned.length;

            var students = [];
            $scope.userProfile.groupsOwned.forEach(function(g) {
                students = students.concat(g.students);
            });

            students = _.uniq(students);

            $scope.studentCount = students.length;
        }

        /**
         * Makes API Call to fetch te full public user profile
         * Stores the result in $scope.userProfile.
         */
        function fetchFullUserProfile() {

            $translate("messages.fetching_profile").then(function(fetchingText){
                fetchingText = '<p>'+fetchingText+'</p><ion-spinner></ion-spinner>';
                $ionicLoading.show({template:fetchingText});
            });


            userServices.getProfile($stateParams.userId)

                .then(function (result) {

                    $scope.userProfile = result.data;
                    $scope.userProfile.humanBirthday = $moment(result.data.birthday).format("MMMM Do YYYY");

                    if (hasRole('student')) {
                        computeLowestRank();
                    }

                    if (hasRole('teacher')) {
                        computeStats();
                    }

                    //Assign avatar profile picture if different from null. Otherwise stay with placeholder
                    if ($scope.userProfile.avatarURL != null && $scope.userProfile.avatarURL != "") {
                        $scope.avatarURI = $scope.userProfile.avatarURL;
                    }
                    else {
                        $scope.avatarURI = $rootScope.gfConfig.avatarPlaceholder;
                    }

                    if ($scope.userProfile.student.house) $scope.userProfile.houseLogo = $scope.userProfile.student.house.logo;
                    //else $scope.userProfile.houseLogo = $rootScope.gfConfig.baseURL + $rootScope.gfConfig.avatarPlaceholder;

                    $ionicLoading.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                })
                .catch(function (err) {

                    $translate("messages.embarrassing_error").then(function(errorText){
                        $ionicLoading.hide();
                        errorText = '<p>' + errorText + '</p>';
                        $ionicLoading.show({template: errorText, duration:2000});
                        errorServices.logError(err, "fetching full user profile");
                    });
                })
        }


        $scope.takePicture = function () {

            cameraServices.takePicture()
                .then(function(nativeURL){
                    return awsServices.uploadToS3(nativeURL);
                })
                .then(function(uploadedFileURL){
                    var profileData = {_id:$scope.userProfile.id, avatarURL:uploadedFileURL};
                    userServices.updateAvatarURL(profileData);
                })
                .catch(function(err){
                    $translate("messages.error_loading_picture").then(function(text){
                        text = '<p>'+text+'</p>';
                        $ionicLoading.show({template:text, duration: 700});
                    });
                })

        };

        $scope.selectPicture = function () {

            cameraServices.selectPicture()
                .then(function(nativeURL){
                    return awsServices.uploadToS3(nativeURL);
                })
                .then(function(uploadedFileURL){
                    var profileData = {_id:$scope.userProfile.id, avatarURL:uploadedFileURL};
                    userServices.updateAvatarURL(profileData);
                })
                .catch(function(err){
                    $translate("messages.error_loading_picture").then(function(text){
                        text = '<p>'+text+'</p>';
                        $ionicLoading.show({template:text, duration: 700});
                    });
                })

        };

        $scope.queueMessage = function (){

            $translate("messages.queuing_message").then(function(text){
                text = '<p>'+text+'</p>';
                $ionicLoading.show({template:text});
            });

            $scope.newMessage.from = authenticationServices.userProfile.id;
            $scope.newMessage.to = $stateParams.userId;
            $scope.modal.hide();

            messagesServices.queue($scope.newMessage)
                .then(function (result) {

                    $scope.newMessage = {};
                    $ionicLoading.hide();
                    $translate("messages.message_queued").then(function(text){
                        text = '<p>'+text+'</p>';
                        $ionicLoading.show({template:text, duration: 700});
                    });
                })
                .catch(function (err) {
                    $translate("messages.embarrassing_error").then(function(errorText){
                        $ionicLoading.hide();
                        errorText = '<p>' + errorText + '</p>';
                        $ionicLoading.show({template: errorText, duration:2000});
                        errorServices.logError(err, "fetching full user profile");
                    });
                })

        }

    })
