(function () {
    angular.module('classbuzz.core')
        .factory('coreServices', function ($rootScope, $localStorage, $location, _, apiServices) {
            var services = {

                /**
                 * Gets a Branch by Its register code
                 * @param code
                 * @returns {*}
                 */
                getBranchByCode: function (code) {
                    var endpoint = '/public/branches/registration/' + code;
                    return apiServices.GET(endpoint);
                },

                /**
                 * Checks if ther is a username with given dni
                 * @param code
                 * @returns {*}
                 */
                dniExists: function (doc) {
                    var endpoint = '/public/users/exists/doc/' + doc;
                    return apiServices.GET(endpoint);
                },


                /**
                 * Checks if ther is a username with given username
                 * @param code
                 * @returns {*}
                 */
                usernameExists: function (username) {
                    var endpoint = '/public/users/exists/username/' + username;
                    return apiServices.GET(endpoint);
                }

            };
            return services;
        });

})();





