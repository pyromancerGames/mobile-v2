(function(){
    angular.module('classbuzz.students')

        .controller('StudentsController', function (
            $http, $rootScope, $scope, $socketio, $ionicPopover, $state,
            $moment, $ionicModal, $ionicListDelegate, $ionicLoading, $translate,
            authenticationServices, errorServices, studentsServices) {

            if(!authenticationServices.userIsLoggedIn()) {
                $state.go("app.home");
                return;
            }

            $scope.students = [];
            $scope.getUserStudents = getUserStudents;


            var loggedUser = authenticationServices.getProfile();

            getUserStudents();

            $scope.doRefresh = function () {
                getUserStudents();
            }

            $scope.fromNow = function(date){
                return($moment(date).fromNow());
            };

            $scope.showUserProfile = function (student) {
                $ionicListDelegate.closeOptionButtons(); //Close swipe cards
                $state.go("app.profile", {"userId": student._id});
            }

            function getUserStudents() {
                $translate("messages.fetching_students").then(function(fetchingText){
                    fetchingText = '<p>'+fetchingText+'</p><ion-spinner></ion-spinner>';
                    $ionicLoading.show({template:fetchingText});
                });

                studentsServices.getTeachersStudents(loggedUser.doc)
                    .then(function(result){
                        $ionicLoading.hide();
                        $scope.students = result.data;
                        $scope.$broadcast('scroll.refreshComplete'); // Stop the ion-refresher from spinning
                    })
                    .catch(function (err) {
                        $translate("messages.embarrassing_error").then(function(errorText){
                            $ionicLoading.hide();
                            errorText = '<p>' + errorText + '</p>';
                            $ionicLoading.show({template: errorText, duration:2000});
                            errorServices.logError(err, "trying to fetch teacher students");
                        });
                    });
            }

        })
})();


