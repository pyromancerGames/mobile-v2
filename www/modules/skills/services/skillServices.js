(function () {

    angular.module('classbuzz.skills')

        .factory('skillServices', function ($http, $rootScope, authenticationServices, apiServices) {

            var services = {
                getSkills: function () {
                    var _endpoint = '/skills';
                    return apiServices.GET(_endpoint);
                },

            };

            return services;
        });

})();
