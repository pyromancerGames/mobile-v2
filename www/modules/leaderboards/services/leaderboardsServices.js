(function () {

    angular.module('classbuzz.leaderboards')

        .factory('leaderboardsServices', function ($http, $rootScope, authenticationServices, apiServices) {

            var services = {

                /**
                 * Get events for a customer, lazy loading
                 * @returns {*}
                 */
                getStudentLeaderboard: function (userId) {

                    var customerId = authenticationServices.userProfile.customer;
                    if (typeof userId == "undefined" || userId == null) userId = authenticationServices.userProfile.id;

                    var endpoint = '/statistics/customer/'+ customerId +'/ranking/students/'+userId;
                    return apiServices.GET(endpoint);

                }

            };
            return services;
        });

})();
