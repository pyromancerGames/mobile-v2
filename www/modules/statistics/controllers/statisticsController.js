(function() {
    //'use strict';

    /**
     * @ngdoc function
     * @name classbuzz.statistics
     * @description
     * # Griffin statistics controller
     */
    angular.module('classbuzz.statistics')
        .controller('StatisticsCtrl', ['$rootScope', '$location', '$scope', 'authenticationServices', 'errorServices',
            function ($rootScope, $location, $scope,  authenticationServices, errorServices) {

                if(!authenticationServices.userIsLoggedIn()) {
                    $state.go('login');
                    return;
                }

                // Only available for super-admin or customer-admins
                var userPermission = authenticationServices.getUserPermissionName();

                var viewModel = this;
                viewModel.userMsg = '';
                viewModel.houses = [];

            }])


        .controller('TeacherStatsCtrl', ['$rootScope', '$location', '$scope', '$timeout', 'statisticsServices', 'errorServices', '_',
            function ($rootScope, $location, $scope, $timeout, statisticsServices, errorServices, _) {

                var viewModel = this;

                viewModel.userMsg = '';
                viewModel.getTeacherStatistics = getTeacherStatistics;
                viewModel.showChart = false;
                viewModel.currentYear = new Date().getFullYear().toString();


                function getTeacherStatistics(teacherId) {

                    statisticsServices.getTeacherActivities(teacherId)
                        .then(function(res) {

                            var currentYearData = res.data[viewModel.currentYear];

                            if(currentYearData) {

                                viewModel.labels = _.keys( currentYearData );
                                var aux = [];
                                for (var s in currentYearData) aux.push( _.sum( currentYearData[s] ) )

                                viewModel.data = [aux];
                                viewModel.showChart = true;
                            }

                        })
                        .catch(function(err) {
                            errorServices.logError(err, "trying get teacher statistics");
                        });
                }
            }])


        .controller('StudentStatsCtrl', ['$rootScope', '$location', '$scope', '$timeout', 'statisticsServices', 'errorServices',
            function ($rootScope, $location, $scope, $timeout, statisticsServices, errorServices) {

                var viewModel = this;
                viewModel.userMsg = '';
                viewModel.getStudentReceivedSts = getStudentReceivedSts;
                viewModel.currentYear = new Date().getFullYear().toString();

                function getStudentReceivedSts(studentId) {

                    statisticsServices.getStudentReceivedStatistics(studentId)
                        .then(function(res) {
                            var currentYearData = res.data[viewModel.currentYear];

                            if(currentYearData) {
                                var monthsLength = currentYearData['reward'].length;

                                viewModel.labels = $rootScope.gfConfig.statsMonthLabels.slice(0, monthsLength);

                                var rewards = statisticsServices.filterRewards(currentYearData);
                                var penalties = statisticsServices.filterPenalties(currentYearData);

                                viewModel.data = [
                                    rewards,
                                    penalties
                                ];
                                viewModel.showChart = true;
                            }
                        })
                        .catch(function(err) {
                            errorServices.logError(err, "trying to get student statistics");
                        });
                }

            }])

        .controller('StudentDetailStatisticsCtrl', ['$rootScope', '$location', '$scope', '$timeout', 'statisticsServices', 'errorServices',
            function ($rootScope, $location, $scope, $timeout, statisticsServices, errorServices) {

                var viewModel = this;
                viewModel.userMsg = '';
                viewModel.getStdDetailedStatistics = getStdDetailedStatistics;
                viewModel.currentYear = new Date().getFullYear().toString();

                viewModel.showChart = true;
                viewModel.pieLabels = ["a", "b", "c"];
                viewModel.pieLabels = [];
                viewModel.pieData = [];
                viewModel.pieColours = [];

                function getStdDetailedStatistics(studentId) {

                    statisticsServices.getStudentDetailedStatistics(studentId)
                        .then(function (res) {

                            var achievements = res.data;

                            for(var i = 0; i < achievements.length; i++) {

                                viewModel.pieData.push(achievements [i].total);
                                viewModel.pieLabels.push(achievements [i].achievement.name);
                                viewModel.pieColours.push(achievements [i]['achievement'].colour);
                            }

                            viewModel.showChart = true;
                        })
                        .catch(function (err) {
                            errorServices.logError(err, "trying to get student detailed statistics");
                        });
                }

                function componentToHex(c) {
                    var hex = c.toString(16);
                    return hex.length == 1 ? "0" + hex : hex;
                }

                function rgbToHex(r, g, b) {
                    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
                }
            }])


        .controller('StudentAttendanceStatisticsCtrl', ['$rootScope', '$location', '$scope', '$timeout', 'statisticsServices', 'errorServices',
            function ($rootScope, $location, $scope, $timeout, statisticsServices, errorServices) {

                var viewModel = this;
                viewModel.attendance = [];
                viewModel.showChart = true;

                viewModel.chartLabels = [];
                viewModel.chartData = [];
                viewModel.chartSeries = [];

                viewModel.getStudentAttendanceStats = getStudentAttendanceStats;

                function getStudentAttendanceStats(studentId){
                    statisticsServices.getStudentAttendanceStatistics(studentId)
                        .then(function(stats){
                            viewModel.chartData[0] = stats.data.map(function(s){
                                if (s.present > 0) return (s.present *  100) / (s.present + s.absent);
                                else return 0;
                            });

                            viewModel.chartData[1] = stats.data.map(function(s){
                                if (s.absent > 0) return (s.absent *  100) / (s.present + s.absent);
                                else return 0;
                            });

                            viewModel.chartSeries[0] = "Present %";
                            viewModel.chartSeries[1] = "Absent %";

                            viewModel.chartLabels = stats.data.map( function(s) { return s.monthName } );
                        })
                }

            }])
})();
