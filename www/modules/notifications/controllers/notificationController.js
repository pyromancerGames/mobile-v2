angular.module('classbuzz.notifications')

    .controller('NotificationController', function ($http, $rootScope, $ionicListDelegate, $ionicLoading, $state,
                                                    $socketio, $moment, $scope, notificationServices, authenticationServices,
                                                    errorServices) {

        $scope.notifications = [];
        $scope.loggedUser = authenticationServices.userProfile;
        $scope.moreItemsAvailable = true;
        // $scope.loadMore = loadMore;
        var nextItems = [];


        $scope.doRefresh = doRefresh;
        populateNotifications();

        $scope.fromNow = function (date) {
            return ($moment(date).fromNow());
        };


        $scope.$on('$ionicView.enter', function () {
            if(!authenticationServices.userIsLoggedIn()) {
                $state.go("app.home");
                return;
            }
        });

        $socketio.on('newnotification', function (notification) {
            $scope.notifications.unshift(notification);
        });

        $scope.deleteNotification = function (notification) {
            notificationServices.deleteNotification(notification._id)
                .then(function (result) {
                    var index = $scope.notifications.indexOf(notification);
                    $scope.notifications.splice(index, 1);
                    $rootScope.$broadcast('notificationdeleted');
                })
                .catch(function (err) {
                    errorServices.logError(err, "trying to delete notification");
                });
        };

        $scope.markAsRead = function (notification) {
            notificationServices.markAsRead(notification._id)
                .then(function (result) {
                    notification.isRead = true;
                    $rootScope.$broadcast('notificationread');
                    $ionicListDelegate.closeOptionButtons(); //Close swipe cards
                })
                .catch(function (err) {
                    errorServices.logError(err, "trying to mark message as read");
                });
        };

        $scope.markAllAsRead = function () {

            notificationServices.markAllAsRead()
                .then(function (result) {
                    $ionicListDelegate.closeOptionButtons(); //Close swipe cards
                    $rootScope.$broadcast('resetnotificationcount');

                    $scope.notifications.forEach(function (notification) {
                        notification.isRead = true;
                    });

                    nextItems.forEach(function (notification) {
                        notification.isRead = true;
                    });
                })
                .catch(function (err) {
                    errorServices.logError(err, "trying to mark all messages as read");
                });
        };


        function populateNotifications () {
            notificationServices.getPendingNotifications()
            .then(function (result) {
                $scope.$broadcast('scroll.refreshComplete'); // Stop the ion-refresher from spinning
                $scope.notifications = result.data;
            })
            .catch(function (err) {
                $scope.$broadcast('scroll.refreshComplete'); // Stop the ion-refresher from spinning
                $ionicLoading.hide();
                $ionicLoading.show({
                    template: 'We are embarrassed, there was an error while preloading data for notifications. Please contact support',
                    duration: 2000
                });
                errorServices.logError(err, "trying to preload data for notifications");
            })
        }

        function doRefresh(){
            $scope.notifications = [];
            populateNotifications ();
            // $scope.moreItemsAvailable = true;
            // nextItems = [];
            // preloadNext().then( loadMore () );
        }

        // function preloadNext() {
        //     $scope.moreItemsAvailable = false;
        //     console.log("Preloading");

        //     return notificationServices.getLazyNotifications($scope.notifications.length)
        //         .then(function (result) {
        //             $scope.$broadcast('scroll.refreshComplete'); // Stop the ion-refresher from spinning
        //             $ionicLoading.hide();

        //             console.log(result.data);

        //             if (result.data.length  == 0) {
        //                 $scope.moreItemsAvailable = false;
        //             }
        //             else {
        //                 $scope.moreItemsAvailable = true;
        //                 nextItems = nextItems.concat(result.data);
        //             }

        //         })
        //         .catch(function (err) {
        //             $scope.$broadcast('scroll.refreshComplete'); // Stop the ion-refresher from spinning
        //             $ionicLoading.hide();
        //             $ionicLoading.show({
        //                 template: 'We are embarrassed, there was an error while preloading data for notifications. Please contact support',
        //                 duration: 2000
        //             });
        //             errorServices.logError(err, "trying to preload data for notifications");
        //         })
        // }


        // function loadMore() {
        //     if (nextItems.length > 0) $scope.notifications.push(nextItems.shift());
        //     if (nextItems.length == 0) preloadNext();
        //     $scope.$broadcast('scroll.infiniteScrollComplete');
        // }


    });


