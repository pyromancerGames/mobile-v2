(function () {
    angular.module('classbuzz.ranks')
        .factory('rankServices', function ($rootScope, $localStorage, $location, _, apiServices) {
            var services = {

                getRanks: function () {
                    var _endpoint = '/ranks';
                    return apiServices.GET(_endpoint);
                }
            };
            return services;
        });

})();


