(function () {

    'use strict';

    angular.module('classbuzz.aws')

        .factory('awsServices', function ($http, $rootScope, authenticationServices, apiServices, errorServices,
                                          $cordovaFileTransfer, cbUtils) {

            var services = {};

            /**
             * Get S3 Credentials
             * @returns {*}
             */
            services.getS3Credentials = function(mimeType){

                    if (typeof mimeType == "undefined" || mimeType == null) mimeType = 'image/jpeg';

                    var _data = {mimeType: mimeType};
                    var _endpoint = "/s3/policy/";
                    return apiServices.POST(_endpoint, _data);
            };

            /**
             *
             * @param fileURI
             * @param serverFileName
             * @param s3Params
             * @param [mimeType] defaults to 'image/jpeg'
             * @returns
             *      {
             *          fileKey: string,
             *          fileName: T,
             *          chunkedMode: boolean,
             *          mimeType: *,
             *          params: {
             *              key: string,
             *              acl: string,
             *              Content-Type: string,
             *              AWSAccessKeyId: *,
             *              success_action_status: string,
             *              Policy: *,
             *              Signature: *
             *          }
             *      }
             */
            services.getUploadOptions = function (fileURI, serverFileName, s3Params, mimeType){

                if (typeof mimeType == 'undefined' || mimeType == null) mimeType = 'image/jpeg';

                return {

                    fileKey: "file",
                    fileName: fileURI.split('/').pop(),
                    chunkedMode: false,
                    mimeType: mimeType,

                    params: {
                        'key' : 'uploads/'+ serverFileName,
                        'acl' : 'public-read-write',
                        'Content-Type' : mimeType,
                        'AWSAccessKeyId': s3Params.AWSAccessKeyId,
                        'success_action_status' : '201',
                        'Policy' : s3Params.s3Policy,
                        'Signature' : s3Params.s3Signature
                    }

                }
            };

            /**
             * Uploads file to S3
             * @param fileURI The local URI of the file to upload
             * @returns {Promise}
             */
            services.uploadToS3 = function (fileURI) {

                return new Promise(function(resolve, reject){

                    services.getS3Credentials()

                        .then( function(s3Params) {

                            //Make the new filename
                            var serverFileName = cbUtils.makeId() +'.'+ fileURI.split('.').pop();
                            var options = services.getUploadOptions(fileURI, serverFileName, s3Params);

                            $cordovaFileTransfer.upload("http://" + $rootScope.gfConfig.s3bucket + ".s3.amazonaws.com/", fileURI, options)
                                .then(function (result) {
                                    var uploadedFileURL = "http://" + $rootScope.gfConfig.s3bucket + ".s3.amazonaws.com/uploads%2F" + serverFileName;
                                    resolve(uploadedFileURL);

                                }, function (err) {
                                    reject(err);
                                })
                        })
                        .catch( function(err) {
                            reject(err);
                        })
                });
            }

            return services;
        });

})();



