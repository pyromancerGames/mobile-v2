angular.module('classbuzz.parents')

    .controller('ChildrenStatController', function ($http, $rootScope, $ionicListDelegate, $ionicLoading, $state, $translate, $ionicHistory,
                                                authenticationServices, userServices, $ionicModal, $socketio, $stateParams, _,
                                                $moment, $scope, $ionicPopup, messagesServices, errorServices, rankServices, statisticsServices) {


        fetchFullUserProfile();
        fetchRanks();
        getUserActivities();
        getUserAttendance();

        $scope.ranks = [];
        $scope.moment = $moment;
        $scope.rankByOrdinal = rankByOrdinal;

        $scope.goBack = function(){
            $ionicHistory.goBack();
        }

        $scope.attendance = {
            'labels': [],
            'series': [],
            'data': [[],[]],
            'colors': [
                {
                  fillColor: "rgba(41, 171, 226, 1)",
                  strokeColor: "rgba(41, 171, 226, 0)"
                },
                {
                  fillColor: 'rgba(255,0,102, 1)',
                  strokeColor: 'rgba(255,0,102, 0)'
                }
            ],
            'options': {
                defaultFontFamily: 'AmsiPro',
                defaultFontSize: '5',
                barShowStroke : false,
                barDatasetSpacing : 0,
                legend: {
                    display: true,
                    position: 'bottom'
                }
            }
        };
        $scope.activities = {
            'labels': [],
            'series': [],
            'data': [[]],
            'colors': [
                {
                  fillColor: 'rgba(255,0,102, 1)',
                  pointColor: 'rgba(255,0,102, 1)',
                  strokeColor: 'rgba(255,0,102, 1)',
                }
            ],

            'options': {
                defaultFontFamily: 'AmsiPro',
                defaultFontSize: '5',
                scaleShowGridLines: true,
                pointDot: false,
                bezierCurve: false,
                legend: {
                    display: true,
                    position: 'top'
                }
            }
        };

        function rankByOrdinal(ordinal) {
            return _.find($scope.ranks, function(r) {
                return r.ordinality == ordinal;
            })
        }

        function dbLocaleTranslate(obj, lang) {
            var _loc = _.filter(obj.locales, {lang:lang});
            var _res = obj;

            _loc.forEach(function(l){
                _res[l.field] = l.value;
            });

            return _res;
        }


        function fetchRanks(){
            rankServices.getRanks()
                .then(function(result){

                    result.data.forEach(function(r){
                        //TODO Translate to user lang
                        var _res = dbLocaleTranslate(r, 'es');
                        $scope.ranks.push(_res);
                    })

                })
        }

        function computeLowestRank() {
            var lowestSkill = _.minBy($scope.userProfile.student.character.skills, function(s) {
                return s.rankInfo.rank;
            })

            $scope.lowestRank = rankByOrdinal(lowestSkill.rankInfo.rank);
        }

        /**
         * Makes API Call to fetch te full public user profile
         * Stores the result in $scope.userProfile.
         */
        function fetchFullUserProfile() {

            $translate("messages.fetching_profile").then(function(fetchingText){
                fetchingText = '<p>'+fetchingText+'</p><ion-spinner></ion-spinner>';
                $ionicLoading.show({template:fetchingText});
            });


            userServices.getProfile($stateParams.userId)

                .then(function (result) {

                    $scope.userProfile = result.data;

                    computeLowestRank();

                    $scope.userProfile.student.character.skills.forEach(function(s){
                        //TODO Translate to user lang
                        s.skill = dbLocaleTranslate(s.skill, 'es');
                    })

                    //Assign avatar profile picture if different from null. Otherwise stay with placeholder
                    if ($scope.userProfile.avatarURL != null && $scope.userProfile.avatarURL != "") {
                        $scope.avatarURI = $scope.userProfile.avatarURL;
                    }
                    else {
                        $scope.avatarURI = $rootScope.gfConfig.avatarPlaceholder;
                    }

                    if ($scope.userProfile.student.house) $scope.userProfile.houseLogo = $scope.userProfile.student.house.logo;
                    //else $scope.userProfile.houseLogo = $rootScope.gfConfig.baseURL + $rootScope.gfConfig.avatarPlaceholder;

                    $ionicLoading.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                })
                .catch(function (err) {

                    $translate("messages.embarrassing_error").then(function(errorText){
                        $ionicLoading.hide();
                        errorText = '<p>' + errorText + '</p>';
                        $ionicLoading.show({template: errorText, duration:2000});
                        errorServices.logError(err, "fetching full user profile");
                    });
                })
        }

        function getUserAttendance () {
            statisticsServices.getStudentAttendance($stateParams.userId)
            .then(function(result){

                $scope.attendance['series'] = ["Presente", "Ausente"];

                result.data.forEach(function(a, idx, arr){
                    $scope.attendance['labels'][idx] = a.monthName;
                    $scope.attendance['data'][0][idx] = a.present;
                    $scope.attendance['data'][1][idx] = a.absent;

                    // $scope.attendance['data'][0][idx] = Math.floor(Math.random() * 100);
                    // $scope.attendance['data'][1][idx] = Math.floor(Math.random() * 100);

                    var all = $scope.attendance['data'][0][idx] + $scope.attendance['data'][1][idx];

                    if (all > 0) {
                        $scope.attendance['data'][0][idx] = $scope.attendance['data'][0][idx] * 100 / all;
                        $scope.attendance['data'][1][idx] = $scope.attendance['data'][1][idx] * 100 / all;
                    }
                })

                // console.log($scope.attendance);
            })
        }

        function getUserActivities () {
            statisticsServices.getStudentActivity($stateParams.userId)
            .then(function(result){
                $scope.activities['series'] = ["Actividades"];

                result.data.forEach(function(a, idx, arr){
                    $scope.activities['labels'][idx] = a.month;
                    $scope.activities['data'][0][idx] = a.activities.length;
                    // $scope.activities['data'][0][idx] = Math.floor(Math.random() * 30);
                })

            })
        }

    });


