(function () {
    angular.module('classbuzz.authentication')
        .factory('authenticationServices', function ($rootScope, $localStorage, $location, _, jwtHelper, apiServices) {

            var AuthenticationServices = {

                userToken: '',
                userTokenLastUpdate: undefined,
                userProfile: {},

                recoverPassword: function (userEmail) {
                    if (typeof userEmail == "undefined" || userEmail == null) return Promise.reject(new Error("Email can't be null."));
                    var data = { email: userEmail };
                    var endpoint = '/public/credentials/recovery';
                    return apiServices.POST(endpoint, data);
                },

                hasRole: function(roleName){

                    //If the user dont have a roles array then obviously dont have the role.
                    if (!AuthenticationServices.userProfile.roles) return false;
                    var names = AuthenticationServices.userProfile.roles.map(function (r) {
                        return r.name
                    });
                    return names.indexOf(roleName) != -1;
                },

                getToken: function () {

                    if (AuthenticationServices.userToken) {
                        if (_.isEmpty(AuthenticationServices.userProfile)) {
                            AuthenticationServices.setProfile(AuthenticationServices.userToken);
                        }
                        return AuthenticationServices.userToken;
                    } else {
                        // Undefined if not exists
                        var newToken = $localStorage.gfAppUserToken;
                        if ($localStorage.gfAppUserToken) {
                            AuthenticationServices.userToken = newToken;
                            AuthenticationServices.setProfile(newToken);
                        }
                        return newToken;
                    }
                },

                logOut: function () {
                    $localStorage.gfAppUserToken = '';
                    AuthenticationServices.userToken = '';
                    AuthenticationServices.userProfile = {};
                    $location.path('/');
                },

                logIn: function (user) {
                    var endpoint = '/public/authenticate';
                    var data = {'username': user.username, 'password': user.password};
                    return apiServices.POST(endpoint,data);
                },

                setProfile: function (token) {
                    // Set Profile
                    var usrProf = jwtHelper.decodeToken(token);
                    // Append may grant of union
                    usrProf = AuthenticationServices.setMayGrantPermissions(usrProf);
                    AuthenticationServices.userProfile = usrProf;
                    $rootScope.user = AuthenticationServices.userProfile;

                    var endpoint = '/users/' + AuthenticationServices.userProfile.id;
                    apiServices.GET(endpoint)
                        .then(function(res) {
                            AuthenticationServices.userProfile.avatarURL = res.data.avatarURL;
                        })
                        .catch(function(err) {
                            console.log("Error loading avatarURL from full profile", err);
                        })

                },

                getProfile: function () {
                    // Decode jwt and if not in memory decode from localstorage call setProfile and return
                    if (_.isEmpty(AuthenticationServices.userProfile)) {
                        if (AuthenticationServices.userIsLoggedIn()) {
                            AuthenticationServices.setProfile(AuthenticationServices.getToken());
                            apiServices.GET(endpoint)
                                .then(function(res) {
                                    AuthenticationServices.userProfile.avatarURL = res.data.avatarURL;
                                })
                                .catch(function(err) {
                                    console.log("Error loading avatarURL from full profile", err);
                                })
                        } else {
                            // TODO: catch error
                            console.log("User not logged in or token unavailable");
                            return undefined;
                        }
                    }

                    return AuthenticationServices.userProfile;
                },


                setToken: function (newToken) {
                    AuthenticationServices.userToken = newToken;
                    $localStorage.gfAppUserToken = newToken;
                    AuthenticationServices.userTokenLastUpdate = new Date();
                },

                setMayGrantPermissions: function (user) {

                    var mayGrant = [];

                    user.roles.forEach(function (r) {
                        mayGrant = _.union(mayGrant, r.mayGrant);
                    });

                    user.mayGrant = mayGrant;

                    return user;
                },

                userIsLoggedIn: function () {

                    if (AuthenticationServices.getToken()/* && !jwtHelper.isTokenExpired(AuthenticationServices.userToken)*/) {
                        return true;
                    } else {
                        return false;
                    }
                },

                // User permissions
                getUserPermissionName: function () {
                    if (!_.isEmpty(AuthenticationServices.userProfile)) {
                        return AuthenticationServices.userProfile.roles[0].name;
                    } else {
                        return undefined;
                    }
                },

                registerUser: function (registerData) {
                    var endpoint = '/public/students/create';
                    var data = {
                        doc: registerData.dni,
                        firstName: registerData.firstName,
                        lastName: registerData.lastName,
                        email: registerData.email,
                        gender: registerData.gender,
                        branch: registerData.branch._id,
                        customer: registerData.branch.customer,
                        birthday: registerData.birthday,
                        credentials: {
                            username: registerData.username,
                            password: registerData.password
                        },
                        student: {
                            parents: []
                        }
                    }

                    if (typeof registerData.parent1.dni != "undefined") {
                        data.student.parents.push({
                            doc: registerData.parent1.dni,
                            firstName: registerData.parent1.firstName,
                            lastName: registerData.parent1.lastName,
                            email: registerData.parent1.email
                        })
                    }

                    if (typeof registerData.parent2.dni != "undefined") {
                        data.student.parents.push({
                            doc: registerData.parent2.dni,
                            firstName: registerData.parent2.firstName,
                            lastName: registerData.parent2.lastName,
                            email: registerData.parent2.email
                        })
                    }

                    return apiServices.POST(endpoint,data);
                },


                registerParent: function (registerData) {
                    var endpoint = '/public/parents/create';
                    var data = {
                        doc: registerData.dni,
                        firstName: registerData.firstName,
                        lastName: registerData.lastName,
                        email: registerData.email,
                        gender: registerData.gender,

                        credentials: {
                            username: registerData.username,
                            password: registerData.password
                        }
                    }

                    return apiServices.POST(endpoint, data);
                },

                getUserDoc: function () {

                    if (AuthenticationServices.userIsLoggedIn) {
                        return AuthenticationServices.userProfile.doc;
                    } else {
                        console.log("User not logged in while trying to get user");
                        return null;
                    }
                },

                getUserId: function () {

                    if (AuthenticationServices.userIsLoggedIn) {
                        return AuthenticationServices.userProfile.id;
                    } else {
                        console.log("User not logged in while trying to get user");
                        return null;
                    }
                },

                getCharGender: function () {
                    return AuthenticationServices.userProfile.gender;
                }

            }

            return AuthenticationServices;
        });

})();
