angular.module('classbuzz.messages')

    .controller('MessagesController', function ($http, $rootScope, $ionicListDelegate, $ionicLoading, $state,
                                                authenticationServices, userServices, $ionicModal, $socketio,
                                                $moment, $scope, $ionicPopup, messagesServices, errorServices) {




        $scope.board = [];
        $scope.contacts = [];
        $scope.showSearch = false;
        $scope.search = {term: ""};

        getMessageboard();

        $scope.toggleSearch = function() {
            $scope.showSearch = !$scope.showSearch;
            $scope.search.term = "";
        };

        $socketio.on('message.new', function (msg) {
            getMessageboard();
        });

        $scope.goTo = function(route, user1, user2){
            $state.go(route, {"user1": user1, "user2": user2});
        }

        $scope.fromNow = function (date) {
            return ($moment(date).fromNow());
        };

        $scope.isMe = function(user){
            if (!authenticationServices.userIsLoggedIn()) return false;
            return user._id.toString() == authenticationServices.userProfile.id.toString();
        };

        $scope.newConversation = function(user) {
            $scope.modal.hide();
            $state.go('app.messagesDetail', {"userId": user._id});
        };

        $scope.selectContact = function() {
            $ionicModal.fromTemplateUrl('modules/messages/views/selectContactModal.html', {scope: $scope})
                .then(function (modal) {
                    getContacts();
                    $scope.modal = modal;
                    $scope.modal.show();
                });
        };

        $scope.$on('$ionicView.enter', function() {
            getMessageboard();
        });

        $scope.filterMessage = function(message)  {

            if (typeof message !== 'object' || message == null) return false;

            var reTerm = $scope.search.term;

            var terms = reTerm.split(" ");

            if (terms.length > 1) {
                reTerm = terms[0];
                for (var i = 1; i < terms.length; i++ ) {
                    reTerm = reTerm + "|" + terms[i]  ;
                }
            }
            //var re = new RegExp(req.query.searchField, "i"); // i = case insensitive
            re = new RegExp(reTerm, "i"); // i = case insensitive

            return re.test(message.user.fullName)
        }


        function getMessageboard () {

            messagesServices.getMessageboard(authenticationServices.userProfile.id)
                .then(function(result){

                    $scope.board = [];
                    

                    for (var user in result.data) {

                        var data = {
                            from: result.data[user][0].from,
                            to: result.data[user][0].to,
                            user: JSON.parse(user),
                            preview: result.data[user][result.data[user].length - 1]
                        };

                        $scope.board.push(data);
                    }


                })
        }

        function getContacts() {
            var params = {};

            params.roles = 'student, teacher';

            userServices.getWithRole(params)
            .then(function(result){
                    $scope.contacts = result.data;
                })

        }
    });


