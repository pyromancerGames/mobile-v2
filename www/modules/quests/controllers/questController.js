angular.module('classbuzz.quests')

    .controller('QuestsController', function ($http, $rootScope, $scope, $socketio, $moment, _,
                                                    $ionicModal, $ionicListDelegate, authenticationServices,
                                                    errorServices, questServices, userServices, cbUtils) {


        $scope.fromNow = cbUtils.fromNow;
        $scope.userQuests = [];
        $scope.userCompletedQuests = [];
        $scope.userSpecialEvents = [];
        $scope.userCompletedSpecialEvents = [];
        $scope.sliderIdx = 0; //0=Active; 1=Completed

        populateSpecialEvents();
        populateCompletedSpecialEvents();

        $scope.$on("$ionicView.enter", function(){
            populateQuests();
        })

        $socketio.on('newquestavailable', function (quest) {
            $scope.userQuests.unshift(quest);
        });

        $socketio.on('activitycompleted', function (quest) {
            populateQuests();
        });

        // $socketio.on('questcompleted', function (quest) {
        //     var qIdx = _.findIndex($scope.userQuests, function(q) {
        //          return quest._id.toString() == q._id.toString();
        //     })

        //     if (qIdx != -1) {
        //         var q = $scope.userQuests.splice(qIdx,1);
        //         $scope.userCompletedQuests.push(q);
        //     }

        // });

        $scope.setSlider = function(idx) {
            $scope.sliderIdx = idx;
        }

        $scope.showQuestDetail = function(quest){
            $scope.detailQuest = quest;
            $ionicModal.fromTemplateUrl('modules/quests/views/questDetailModal.html', {
                scope: $scope

            }).then(function(modal) {
                $scope.modal = modal;
                $scope.modal.show();
            });

        };

        $scope.showSpecialEventDetail = function(event){
            $scope.eventDetail = event;
            $ionicModal.fromTemplateUrl('modules/quests/views/sEventDetailModal.html', {
                scope: $scope

            }).then(function(modal) {
                $scope.modal = modal;
                $scope.modal.show();
            });

        };

        function populateQuests () {
            userServices.getActiveQuests()
                .then(function(result){
                    $scope.userQuests = result.data;
                })

            userServices.getCompletedQuests()
                .then(function(result){
                    $scope.userCompletedQuests = result.data;
                })
            
        }

        function populateSpecialEvents () {
            userServices.getActiveSpecialEvents()
                .then(function(result){
                    $scope.userSpecialEvents = result.data;
                })
        }

        function populateCompletedSpecialEvents () {
            userServices.getCompletedSpecialEvents()
                .then(function(result){
                    $scope.userCompletedSpecialEvents = result.data;
                })
        }


    })

