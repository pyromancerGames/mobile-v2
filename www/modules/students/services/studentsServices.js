(function() {
    angular.module('classbuzz.students')
        .factory('studentsServices', ['$rootScope',  'apiServices',  function ($rootScope, apiServices) {

            var services = {

                getTeachersStudents: function (teacherDoc) {

                    var endpoint = '/users/'+teacherDoc+'/students';
                    return apiServices.GET(endpoint);

                }

            }
            return services;
        }]);

})();

