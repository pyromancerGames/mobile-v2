angular.module('classbuzz.statistics', ['chart.js'])
    // Optional configuration
    .config(['ChartJsProvider', function (ChartJsProvider) {
        // Configure all charts
        ChartJsProvider.setOptions({
            colours: ['#ed5565', '#ed5565'],
            //#FF8A80
            responsive: true
        });
        // Configure all line charts
        ChartJsProvider.setOptions('Line', {
            datasetFill: false
        });
        ChartJsProvider.setOptions('Line', {
            showTooltips: false
        });

    }]);
