(function () {
    angular.module('classbuzz.activities')
        .factory(
            'activityServices',
            function ($rootScope, $localStorage, $location, _, apiServices,  authenticationServices) {

                var services = {
                    /**
                     * Get activities for a customer
                     * @param [searchFilter] - Search field filter. Defaults to empty string
                     * @param [customerId] - Defaults to logged user's customer
                     * @returns {*}
                     */
                    getCustomerActivites: function (searchFilter, customerId) {

                        if (typeof searchFilter == "undefined" || searchFilter == null) searchFilter = "";
                        if (typeof customerId == "undefined" || customerId == null) customerId = authenticationServices.userProfile.customer;

                        var _data = {
                            q: searchFilter,
                            limit: -1,
                            customer: customerId
                        };

                        var _querystring = _.toQueryString(_data);

                        var endpoint = '/activities' + _querystring;
                        return apiServices.GET(endpoint);
                    },


                };
                return services;
            });
})();



