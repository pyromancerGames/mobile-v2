angular.module('classbuzz.groups')


    .controller('GroupStudentsController', function (
        $http, $rootScope, $scope, $state, $stateParams, $socketio, $ionicModal,
        $ionicListDelegate, $ionicPopover, $moment, $translate, $q, $cordovaDatePicker, ionicDatePicker,
        $ionicLoading, $timeout, $ionicPopup, authenticationServices, errorServices, newsServices, cameraServices, skillServices,
        groupServices, attendanceServices, userServices, activityServices, awsServices, messagesServices, $ionicHistory, _) {

        $scope.students = [];
        $scope.availableStudents = [];
        $scope.activities = [];
        $scope.activeQuests = [];
        $scope.recentActivities = [];
        $scope.searchField = {value: ""};
        $scope.activitySearchField = {value: ""};
        $scope.todaysAttendance = {};
        $scope.today = moment(new Date()).format('dddd[,] MMMM Do YYYY');
        $scope.selectionMode = false;
        $scope.checkAllModel = { value: false };
        $scope.studentsRewarded = [];
        $scope.news = {};
        $scope.news.info = {};
        $scope.news.info.image = "";
        $scope.group = {};
        $scope.getGroupComplement = getGroupComplement;
        $scope.doRemoveStudent = removeFromGroup;
        $scope.getActivities = getActivities;
        $scope.userProfile = {};
        $scope.editGroupData = [];
        $scope.groupLevels = [];
        $scope.newMessage = {};
        $scope.messageStudent = {};
        $scope.skills = [];
        $scope.rewardSkillFilter = [];
        $scope.penaltySkillFilter = [];


        if (authenticationServices.userIsLoggedIn())
            getFullUserProfile();

        $scope.barStyles = {
            container1: {
                position: "absolute",
                bottom: "-20px",
                left: "0px",
                width: "100%",
                height: "15px"
            },

            bar1: {
                backgroundColor: "#fb8328"
            }
        }

        populateGroup();
        populateSkills();
        loadGroupLevels();

        $scope.toggleRewardSkill = function(skillId) {
            var sIdx = _.findIndex($scope.rewardSkillFilter, {_id:skillId});
            if (sIdx == -1) return;
            $scope.rewardSkillFilter[sIdx].isActive = !$scope.rewardSkillFilter[sIdx].isActive;
            getActivities();
        }

        $scope.togglePenaltySkill = function(skillId) {
            var sIdx = _.findIndex($scope.penaltySkillFilter, {_id:skillId});
            if (sIdx == -1) return;
            $scope.penaltySkillFilter[sIdx].isActive = !$scope.penaltySkillFilter[sIdx].isActive;
            getActivities();
        }

        // $scope.openDatePicker = function() {
        //     //Will fail in browser. To be tested in emulator or actual device.
        //     var options = {
        //         date: new Date(),
        //         mode: 'date', // or 'time'
        //         minDate: moment().startOf('week'),
        //         allowOldDates: true,
        //         allowFutureDates: false,
        //         doneButtonLabel: 'DONE',
        //         doneButtonColor: '#F8F8F8',
        //         cancelButtonLabel: 'CANCEL',
        //         cancelButtonColor: '#616161'
        //     };

        //     $cordovaDatePicker.show(options).then(function(date){
        //         getAttendance(moment(date).startOf('day'));
        //     });

        // };

    
         $scope.openDatePicker = function() {

            var initData = {

                callback: function (val) {  //Mandatory
                    getAttendance(moment(val).startOf('day'));
                },

                from: moment().startOf('week'),
                to: moment().endOf('week'),
                inputDate: new Date(),      //Optional
                mondayFirst: true,          //Optional
                disableWeekdays: [0,6],     //Optional
                closeOnSelect: false,       //Optional
                templateType: 'popup'       //Optional
            };

            ionicDatePicker.openDatePicker(initData);
        };


        $scope.barAction = function () {
            
            if ($scope.selectionMode) {
                $scope.selectionModeOff();
                return;
            } 
            else {
                $scope.goBack();
            }
            
        }

        $scope.checkUncheckAll = function(){
            $scope.students.forEach(function(s){
                s.isSelected = $scope.checkAllModel.value;
            })

            $scope.checkAllModel.value = !$scope.checkAllModel.value;
        };

        $scope.fromNow = function(date){
            return($moment(date).fromNow());
        };

        $scope.toggleSelectionMode = function(){
            $scope.selectionMode = !$scope.selectionMode;
            $scope.checkAllModel.value = false;
            $scope.studentsRewarded = [];
        };

        $scope.selectionModeOn = function(heldUser){
            $scope.selectionMode = true;
            $scope.checkAllModel.value = true;
            $scope.studentsRewarded = [];
            heldUser.isSelected = true;
        };

        $scope.selectionModeOff = function(){
            $scope.selectionMode = false;
            $scope.checkAllModel.value = false;
            $scope.students.forEach(function(s){
                s.isSelected = false
            })
            $scope.studentsRewarded = [];
        };

        $scope.editMode = function(heldUser) {
            if (!$scope.selectionMode) $scope.selectionModeOn(heldUser);
            if ($scope.selectionMode) heldUser.isSelected = true;
        }

        $scope.goBack = function(){
            $ionicHistory.goBack();
        };

        $scope.addActivityWithConfirm = function(activity) {

            $translate(['titles.confirm', 'messages.confirm_grant']).then(function(trans){
                var confirmPopup = $ionicPopup.confirm({
                    title: trans['titles.confirm'],
                    template: trans['messages.confirm_grant'] +': '+ activity.name+'?'
                });

                confirmPopup.then(function(res) {
                    if(res) {
                        doAddActivity(activity);
                    }
                });
            });
        };

        $scope.createNewsWithConfirm = function() {

            $translate(['titles.confirm', 'messages.confirm_share_news']).then(function(trans){
                var confirmPopup = $ionicPopup.confirm({
                    title: trans['titles.confirm'],
                    template: trans['messages.confirm_share_news'] + '?'
                });

                confirmPopup.then(function(res) {
                    if(res) {
                        createNews();
                    }
                });
            });

        };

        $scope.addStudent = function () {
            $scope.modal = $scope.selectStudentModal;
            getGroupComplement();
            $scope.modal.show();
        };

        $scope.doRefresh = function () {
            populateGroup();
        }

        $scope.closeModal = function () {
            $scope.modal.hide();
            resetModalVars();
            $ionicListDelegate.closeOptionButtons(); //Close swipe cards
            populateGroup();
        }

        $scope.showUserProfile = function (student) {
            $ionicListDelegate.closeOptionButtons(); //Close swipe cards
            $state.go("app.profile", {"userId": student._id});
        }

        $scope.showRewardModal = function (student) {

            if (student != null) {
                $scope.studentsRewarded = [];
                $scope.studentsRewarded.push(student);
            }
            else
            {   
                $scope.studentsRewarded = [];
                $scope.students.forEach(function(s){
                    if (s.isSelected) $scope.studentsRewarded.push(s);
                });

            }

            if ($scope.studentsRewarded.length == 0) return;

            getActivities();

            //If single reward then show all user specific information
            if ($scope.studentsRewarded.length == 1) {
                $scope.getActiveQuests();
                $scope.getRecentActivities();
            }

            $scope.modal = $scope.feedBackModal;
            $scope.modal.show();
        }

        //TODO Maybe have a separate controller for modal?
        $scope.showAttendanceModal = function () {
            getAttendance();
            $scope.modal = $scope.attendanceModal;
            $scope.modal.show();
        }

        // Message modal
        $scope.openSendMessage = function (student) {
            messageStudent = student;
            $scope.modal = $scope.sendMessageModal;
            $scope.modal.show();
        }

        $scope.queueMessage = function (student){


            $translate("messages.queuing_message").then(function(text){
                text = '<p>'+text+'</p>';
                $ionicLoading.show({template:text});
            });

            $scope.newMessage.from = authenticationServices.userProfile.id;
            $scope.newMessage.to = messageStudent._id;
            $scope.modal.hide();

            messagesServices.enqueue($scope.newMessage)
                .then(function (result) {
                    $scope.newMessage = {};
                    $ionicLoading.hide();
                    $translate("messages.message_queued").then(function(text){
                        text = '<p>'+text+'</p>';
                        $ionicLoading.show({template:text, duration: 700});
                    });
                })
                .catch(function (err) {
                    $translate("messages.embarrassing_error").then(function(errorText){
                        $ionicLoading.hide();
                        errorText = '<p>' + errorText + '</p>';
                        $ionicLoading.show({template: errorText, duration:2000});
                        errorServices.logError(err, "fetching full user profile");
                    });
                })

        };

        $scope.showNewsModal = showNewsModal;

        function showNewsModal (imageURL) {

            //if ( _.isNil(imageURL) ) imageURL = "";

            $scope.news.info.image = imageURL;
            $scope.modal = $scope.createNewsModal;
            $scope.modal.show();
        }

        $scope.clearSelectStudentsModalFilters = function(){
            $scope.searchField.value = "";
            $scope.getGroupComplement();

        }

        $scope.getBranchStudents = function() {
            $scope.getGroupComplement();
        }

        $scope.clearSelectActivityModalFilters = function(){
            $scope.activitySearchField.value= "";
            $scope.getAchievements();
        }

        $scope.cycleAttendance = function(att) {
            var states = ['present', 'late', 'absent', 'canceled', 'certified'];
            var current = states.indexOf(att.status);
            var next = (current + 1) % 5;
            att.status = states[next];
        }

        $scope.setAllAsPresent = function () {
            $scope.todaysAttendance.students.forEach(function (s){
                s.status = 'present';
            })
        }

        $scope.setAllAsAbsent = function () {
            $scope.todaysAttendance.students.forEach(function (s){
                s.status = 'absent';
            })
        }

        $scope.setAllAsCanceled = function () {
            $scope.todaysAttendance.students.forEach(function (s){
                s.status = 'canceled';
            })
        }


        //<editor-fold desc="POPOVERS ==">
        //Create the popovers
        $ionicPopover.fromTemplateUrl('modules/groups/views/groupStudentsPopover.html', {scope: $scope})
            .then(function(popover) {
                $scope.popover = popover;
            });

        $scope.openPopover = function($event) {
            $scope.popover.show($event);
        };

        $scope.closePopover = function() {
            $scope.popover.hide();
        };

        //Cleanup the popover when we're done with it!
        $scope.$on('$destroy', function() {
            $scope.popover.remove();
        });

        // Execute action on hide popover
        $scope.$on('popover.hidden', function() {
            // Execute action
        });
        // Execute action on remove popover
        $scope.$on('popover.removed', function() {
            // Execute action
        });

        //</editor-fold>

        //<editor-fold desc="MODAL DEFINITION ==">

        // Create the news modal
        $ionicModal.fromTemplateUrl('modules/groups/views/createNewsModal.html', {scope: $scope})
            .then(function (modal) {
                $scope.createNewsModal = modal;
            });


        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('modules/groups/views/editGroup.html', {scope: $scope})
            .then(function (modal) {
                $scope.editGroupModal = modal;
            });

        // Create the selectStudents modal
        $ionicModal.fromTemplateUrl('modules/groups/views/selectStudents.html', {scope: $scope})
            .then(function (modal) {
                $scope.selectStudentModal = modal;
            });

        // Create the attendanceModal
        $ionicModal.fromTemplateUrl('modules/groups/views/attendanceModal.html', {scope: $scope})
            .then(function (modal) {
                $scope.attendanceModal = modal;
            });

        // Create the feedback modal
        $ionicModal.fromTemplateUrl('modules/groups/views/feedbackModal.html', {scope: $scope})
            .then(function (modal) {
                $scope.feedBackModal = modal;
            });

        // Create the send mail modal
        $ionicModal.fromTemplateUrl('modules/users/views/sendParentMessageModal.html', {scope: $scope})
            .then(function (modal) {
                $scope.sendMessageModal = modal;
            });
        //</editor-fold>

        //<editor-fold desc="API CALLS ==">
        /**
         * Gets the initial set of students assigned to the current group.
         * Upon resolving promise, the function updates the $scope.students variable
         */
        function populateGroup() {

            $translate("messages.fetching_students").then(function(fetchingText){
                fetchingText = '<p>'+fetchingText+'</p><ion-spinner></ion-spinner>';
                $ionicLoading.show({template:fetchingText});
            });

            groupServices.getGroup($stateParams.groupId)
                .then(function(gp) {

                    $scope.group = gp.data;

                    groupServices.getGroupStudents($stateParams.groupId)

                        .then(function (students) {
                            $ionicLoading.hide();
                            $scope.students = [];
                            students.data.forEach(function (s) {
                                s.isSelected = false;
                                $scope.students.push(s);
                            });
                            $scope.$broadcast('scroll.refreshComplete'); // Stop the ion-refresher from spinning
                        })
                })
                .catch(function (err) {

                    $translate("messages.embarrassing_error").then(function(errorText){
                        $ionicLoading.hide();
                        errorText = '<p>' + errorText + '</p>';
                        $ionicLoading.show({template: errorText, duration:2000});
                        errorServices.logError(err, "trying to populate group");
                    });

                });
        }


        function populateSkills () {
            skillServices.getSkills()
            .then(function(result){
                $scope.skills = result.data;
                
                $scope.rewardSkillFilter = $scope.skills.map(function(s){
                    s.isActive = true;
                    return s;
                })

                $scope.penaltySkillFilter = $scope.skills.map(function(s){
                    s.isActive = true;
                    return s;
                })

            })
            .catch(function (err) {
                    $translate("messages.embarrassing_error").then(function(errorText){
                        $ionicLoading.hide();
                        errorText = '<p>' + errorText + '</p>';
                        $ionicLoading.show({template: errorText, duration:2000});
                        errorServices.logError(err, "trying to populate skills on groupStudentController");
                    });

                });
        }

        /**
         * Gets attendance for given Date.
         * @param [date] - Date defaults to today.
         */
        function getAttendance(date){

            if (typeof date == 'undefined' || date == null) date = moment().startOf('day');

            attendanceServices.getAttendance($stateParams.groupId, date)

                .then(function (att) {
                    $scope.todaysAttendance = att.data;
                    $scope.todaysAttendance.date = date;
                    $scope.todaysAttendance.humanDate = moment(date).format('dddd[,] MMMM Do YYYY');
                })
                .catch(function (err) {
                    errorServices.logError(err, "trying to get attendance for the day");
                });
        }

        /**
         * Updates Attendance from scope vars.
         */
        $scope.updateAttendance = function(){

            //Remove Avatar URL from request. To prevent Entity too large problem.
            $scope.todaysAttendance.students.forEach(function(s){
                delete s.student["avatarURL"];
            });

            attendanceServices.updateAttendance($scope.todaysAttendance)
                .then(function (att) {
                    $scope.closeModal();
                })
                .catch(function (err) {
                    errorServices.logError(err, "trying to update attendance");
                });
        };


        /**
         * Gets all the active quests for the current user.
         * Upon resolving promise, the function updates the $scope.activeQuests variable
         */
        $scope.getActiveQuests = function () {

            userServices.getActiveQuests($scope.studentsRewarded[0]._id)

                .then(function (quests) {
                    $scope.activeQuests = quests.data;
                })
                .catch(function (err) {
                    errorServices.logError(err, "trying to get quests");
                });
        };

        /**
         * Gets recent given activites for the current user.
         * Upon resolving promise, the function updates the $scope.recentActivites variable
         */
        $scope.getRecentActivities = function () {

            userServices.getRecentActivities($scope.studentsRewarded[0]._id, 3)
                .then(function (result) {
                    $scope.recentActivities = result.data;
                })
                .catch(function (err) {
                    errorServices.logError(err, "trying to get recent activities");
                });
        };

        // Display the edit group modal
        $scope.editGroup = function (group) {
            $scope.editGroupData = group;
            $scope.modal = $scope.editGroupModal;
            $scope.modal.show();
        };

        // Update group
        $scope.doEditGroup = function () {

            groupServices.updateGroup($scope.editGroupData)
                .then(function (result) {
                    $scope.closeModal();
                    $scope.editGroupData = {};
                })
                .catch(function (err) {
                    errorServices.logError(err, "trying to edit group");
                });
        };

        /**
         * Load groups levels to display in the edit view
         */
        function loadGroupLevels() {

            groupServices.getGroupLevels(authenticationServices.userProfile.customer)

                .then(function (levels) {
                    $scope.groupLevels = levels.data;
                })

                .catch(function (err) {
                    errorServices.logError(err, "trying to fetch group levels");
                });
        }

        /**
         * Adds selected student to current group. The group is contained in the stateParams that was set upon calling the view
         * The student is selected in the modal. After the promise is resolved the student is spliced from the $scope.availableStudents list
         * to prevent being added twice.
         * @param student - student to be added to group
         */
        $scope.doAddStudent = function (student) {

            $translate("messages.adding_student_group").then(function(fetchingText){
                fetchingText = '<p>'+fetchingText+'</p><ion-spinner></ion-spinner>';
                $ionicLoading.show({template:fetchingText});
            });

            groupServices.addToGroup($stateParams.groupId, student._id)

                .then(function (results) {
                    var index = $scope.availableStudents.map(function (s) {
                        return s._id
                    }).indexOf(student._id);
                    $scope.availableStudents.splice(index, 1);
                    $ionicLoading.hide();
                })
                .catch(function (err) {

                    $translate("messages.embarrassing_error").then(function(errorText){
                        $ionicLoading.hide();
                        errorText = '<p>' + errorText + '</p>';
                        $ionicLoading.show({template: errorText, duration:2000});
                        errorServices.logError(err, "trying to add a students");
                    });

                });
        };

        /**
         * Removes student from current group.
         * The group is contained in the stateParams that was set upon calling the view.
         * @param student
         */
        function removeFromGroup (student) {

            groupServices.removeFromGroup($stateParams.groupId, student._id)

                .then(function (results) {
                    var index = $scope.students.map(function (s) {
                        return s._id
                    }).indexOf(student._id);
                    $scope.students.splice(index, 1);
                })
                .catch(function (err) {
                    errorServices.logError(err, "trying to remove a student");
                });
        }


        /**
         * Gets all the students from current branch that are still not linked to the working group.
         * The group is contained in the stateParams that was set upon calling the view
         * Upon resolving promise, the function updates the $scope.availableStudents variable
         */
        function getGroupComplement(){
            $translate("messages.fetching_students").then(function(fetchingText){
                fetchingText = '<p>'+fetchingText+'</p><ion-spinner></ion-spinner>';
                $ionicLoading.show({template:fetchingText});
            });

            groupServices.getGroupComplement($stateParams.groupId, $scope.searchField.value)

                .then(function (students) {
                    $ionicLoading.hide();
                    $scope.availableStudents = students.data;
                })
                .catch(function (err) {
                    $translate("messages.embarrassing_error").then(function(errorText){
                        $ionicLoading.hide();
                        errorText = '<p>' + errorText + '</p>';
                        $ionicLoading.show({template: errorText, duration:2000});
                        errorServices.logError(err, "trying to get branch students");
                    });

                });
        };

        /**
         * Adds activity to currently selected user. The user is stored in a scope variable called studentRewarded
         * @param activity - Achievement to be awarded to the student
         */
        function doAddActivity (activity) {

            $translate("messages.granting_feedback").then(function(fetchingText){
                fetchingText = '<p>'+fetchingText+'</p><ion-spinner></ion-spinner>';
                $ionicLoading.show({template:fetchingText});
            });

            var promises = [];

            $scope.studentsRewarded.forEach(function(s){
                promises.push(userServices.addActivity(s._id, activity._id));

            });

            $q.all(promises)
                .then(function (results) {
                    $scope.activitySearchField.value = "";
                    activity.hasBeenAdded = true;
                    $timeout($scope.closeModal,500);
                    $scope.selectionModeOff();
                    $ionicLoading.hide()
                })
                .catch(function (err) {
                    $translate("messages.error_try_again").then(function(errorText){
                        $ionicLoading.hide();
                        errorText = '<p>' + errorText + '</p>';
                        $ionicLoading.show({template: errorText, duration:2000});
                        errorServices.logError(err, "trying to add activity");
                    });
                });
        };

        /**
         *
         */
        function getActivities (search) {
            activityServices.getCustomerActivites(search)
                .then(function(result){
                    $scope.activities = result.data;
                })
                .catch(function (err) {
                    errorServices.logError(err, "trying to get customer activities");
                });
        }

        function getFullUserProfile () {
            userServices.getProfile()

                .then(function (result) {
                    $scope.userProfile = result.data;
                    $scope.userProfile.humanBirthday = $moment(result.data.birthday).format();

                    //Assign avatar profile picture if different from null. Otherwise stay with placeholder
                    if ($scope.userProfile.avatarURL != null && $scope.userProfile.avatarURL != "") {
                        $scope.avatarURI = $scope.userProfile.avatarURL;
                    }
                    else {
                        $scope.avatarURI = $rootScope.gfConfig.avatarPlaceholder;
                    }

                })
                .catch(function (err) {

                    $translate("messages.embarrassing_error").then(function(errorText){
                        $ionicLoading.hide();
                        errorText = '<p>' + errorText + '</p>';
                        $ionicLoading.show({template: errorText, duration:2000});
                        errorServices.logError(err, "fetching full user profile");
                    });

                })
        }

        function createNews () {


            $translate("messages.creating_post").then(function(fetchingText){
                fetchingText = '<p>'+fetchingText+'</p><ion-spinner></ion-spinner>';
                $ionicLoading.show({template:fetchingText});
            });

            $scope.news.group = $stateParams.groupId;

            //upload to S3 prior to save news if news has image
            if (!_.isNil($scope.news.info.image)) {

                awsServices.uploadToS3($scope.news.info.image)
                    .then(function(uploadedURL){
                        $scope.news.info.image = uploadedURL;
                        return newsServices.createNews($scope.news);
                    })
                    .then(function (result) {
                        $ionicLoading.hide();
                        $scope.closeModal();
                    })
                    .catch(function (err) {

                        $translate("messages.embarrassing_error").then(function(errorText){
                            $ionicLoading.hide();
                            errorText = '<p>' + errorText + '</p>';
                            $ionicLoading.show({template: errorText, duration:2000});
                            errorServices.logError(err, "Creating News Item");
                        });

                    })
            }

            else {
                    newsServices.createNews($scope.news)
                    .then(function (result) {
                        $ionicLoading.hide();
                        $scope.closeModal();
                    })
                    .catch(function (err) {

                        $translate("messages.embarrassing_error").then(function(errorText){
                            $ionicLoading.hide();
                            errorText = '<p>' + errorText + '</p>';
                            $ionicLoading.show({template: errorText, duration:2000});
                            errorServices.logError(err, "Creating News Item");
                        });

                    })
            }

        }

        //</editor-fold>

        //<editor-fold desc="LOCAL FUNCTIONS ==">
        /**
         * Reset all variables used by any modal.
         */
        function resetModalVars(){
            $scope.availableStudents = [];
            $scope.activities = [];
            $scope.searchField = {value: ""};
            $scope.activitySearchField = {value: ""};
            $scope.news.info.text = "";
            $scope.news.info.image = null;
        }

        $scope.takePicture = function () {

            cameraServices.takePicture()
                .then(function(imageURI){
                    showNewsModal(imageURI);
                })
                .catch(function(err){
                    if (err == "Camera cancelled.") return;
                    $scope.news.info.image = "";
                    $translate("messages.error_loading_picture").then(function(text){
                        text = '<p>'+text+'</p>';
                        $ionicLoading.show({template:text, duration: 700});
                    });
                    errorServices.logError(err, 'Taking Picture');
                })
        };

        $scope.selectPicture = function () {

            cameraServices.selectPicture()
                .then(function(imageURI){
                    showNewsModal(imageURI);
                })
                .catch(function(err){

                    $scope.news.info.image = "";
                    $translate("messages.error_loading_picture").then(function(text){
                        text = '<p>'+text+'</p>';
                        $ionicLoading.show({template:text, duration: 700});
                    });

                    errorServices.logError(err, 'Selecting Picture');

                })

        };

        //</editor-fold>


        $scope.filterReward = function(item) {
            var re = new RegExp('.*'+$scope.activitySearchField.value+'.*', "i");
            var skillMatch = false;

            $scope.rewardSkillFilter.forEach(function(s){
                if (s.isActive) skillMatch = skillMatch || hasSkill(item, s);
            });

            return item.reward.xp > 0 && re.test(item.name) && skillMatch;
        }

        $scope.filterPenalty = function(item) {
            var re = new RegExp('.*'+$scope.activitySearchField.value+'.*', "i");
            var skillMatch = false;

            $scope.penaltySkillFilter.forEach(function(s){
                if (s.isActive) skillMatch = skillMatch || hasSkill(item, s);
            });

            return item.reward.xp <= 0 && re.test(item.name) && skillMatch;
        }

        function hasSkill(item, skill) {
            var sIdx = _.findIndex (item.skills, {_id:skill._id});
            return sIdx != -1;
        }

    })

    .filter('capFirstLetter', function() {
        return function(input, scope){
            if (input!=null) {
                input = input.toLowerCase();
                return input.substring(0,1).toUpperCase()+input.substring(1);
            }
        }
    })





