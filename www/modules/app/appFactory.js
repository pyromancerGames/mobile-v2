(function () {
    angular.module('griffinmobile')

        .factory('$socketio', function ($rootScope, socketFactory) {

            var myIoSocket = io.connect($rootScope.gfConfig.baseURL);

            var mySocket = socketFactory({
                ioSocket: myIoSocket
            });

            return mySocket;
        });

})();

