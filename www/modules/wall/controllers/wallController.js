angular.module('classbuzz.wall')

    .controller('WallController', function ($http, $rootScope, $scope, $socketio, $moment, $ionicLoading, $ionicPopup, $cordovaToast,
                                                authenticationServices, errorServices, newsServices, _, $translate, ngFB) {
        $scope.news = [];
        $scope.loggedUser = authenticationServices.userProfile;

        $scope.moreItemsAvailable = true;
        $scope.loadMore = loadMore;
        $scope.addLike = addLike;
        $scope.userLikesPost = userLikesPost;
        $scope.isMyPost = isMyPost;
        $scope.deletePostWithConfirm = deletePostWithConfirm;
        $scope.fbShare = fbShare;

        var nextItems = [];

        $scope.doRefresh = doRefresh;

        $socketio.on('newnews', function(item) {
            $scope.news.unshift(item);
        });

        $scope.fromNow = function(date){
            return($moment(date).fromNow());
        };

        /**
         * Refreshs the list
         */
        function doRefresh(){
            $scope.moreItemsAvailable = true;
            $scope.news = [];
            nextItems = [];
            preloadNext();
        }

        /**
         * Prefetchs the next items to be loaded with infinit scroll
         * @returns {*}
         */
        function preloadNext() {
            $scope.moreItemsAvailable = false;

            return newsServices.getLazyNews($scope.news.length)
                .then(function (result) {
                    $scope.$broadcast('scroll.refreshComplete'); // Stop the ion-refresher from spinning
                    $ionicLoading.hide();

                    if (result.data.length  == 0) {
                        $scope.moreItemsAvailable = false;
                    }
                    else {
                        $scope.moreItemsAvailable = true;
                        nextItems = nextItems.concat(result.data);
                    }

                })
                .catch(function (err) {
                    $scope.$broadcast('scroll.refreshComplete'); // Stop the ion-refresher from spinning
                    $ionicLoading.hide();
                    $ionicLoading.show({
                        template: 'We are embarrassed, there was an error while preloading data for news wall. Please contact support',
                        duration: 2000
                    });
                    errorServices.logError(err, "trying to preload data for news wall");
                })
        }

        /**
         * Infinte scroll. Loads more items.
         */
        function loadMore() {
            if (nextItems.length > 0) $scope.news.push(nextItems.shift());
            if (nextItems.length == 0) preloadNext();
            $scope.$broadcast('scroll.infiniteScrollComplete');
        }

        /**
         * Adds post like.
         * First it checks if user has reacted to post previously. If so, the reaction is removed by this action.
         * Otherwise it adds reaction.
         * Tried to emulate FB like behaviour.
         *
         * @param post
         */
        function addLike(post){

            var userReaction = userLikesPost(post);

            if (userReaction != null) {
                //Already likes this post, remove like
                removeLike(post, userReaction);
                return;
            }


            //Does not like the post add like
            newsServices.addReaction(post._id)
                .then(function(news){

                    var newsIndex = _.findIndex($scope.news, function(n){
                        return n._id == news.data._id;
                    });

                    $scope.news[newsIndex].reactions = news.data.reactions;
                })
                .catch(function (err) {
                    $ionicLoading.show({
                        template: 'We are embarrassed, there was an error adding like.',
                        duration: 2000
                    });
                    errorServices.logError(err, "trying to add reaction for news wall");
                })
        }

        /**
         * Removes post like.
         * @param post
         * @param reaction
         */
        function removeLike(post, reaction){

            newsServices.removeReaction(post._id, reaction._id)

                .then(function(news){

                    var newsIndex = _.findIndex($scope.news, function(n){
                        return n._id == news.data._id;
                    });

                    $scope.news[newsIndex].reactions = news.data.reactions;
                })
                .catch(function (err) {
                    $ionicLoading.show({
                        template: 'We are embarrassed, there was an error adding like.',
                        duration: 2000
                    });
                    errorServices.logError(err, "trying to add reaction for news wall");
                })
        }

        /**
         * Returns user reaction if user already has reacted to post received
         * False otherwise.
         * @param post
         * @returns {*}
         */
        function userLikesPost(post) {

            var userLikeIndex = _.findIndex(post.reactions, function(r){
                return (r.type == 'like' && r.author == authenticationServices.userProfile.id);
            });

            if (userLikeIndex != -1) return post.reactions[userLikeIndex];
            else return null;
        }

        /**
         * Returns true if the post received was created by logged user.
         * False otherwise.
         * @param post
         * @returns {boolean}
         */
        function isMyPost(post) {
            return post.author._id == authenticationServices.userProfile.id
        }

        /**
         * Prompts if user wants to delete post.
         * @param post Object - Post Object
         */
        function deletePostWithConfirm(post){
            $translate(['titles.confirm', 'messages.confirm_delete']).then(function(trans){
                var confirmPopup = $ionicPopup.confirm({
                    title: trans['titles.confirm'],
                    template: trans['messages.confirm_delete']
                });

                confirmPopup.then(function(res) {
                    if(res) {
                        deletePost(post);
                    }
                });
            });
        }

        /**
         * Deletes News Post.
         * After the promise returns, removes the post from the array.
         * @param post Object - Post Object
         */
        function deletePost(post) {
            newsServices.removePost(post)

                .then(function(news){

                    var postIndex = _.findIndex( $scope.news, function(p){
                        return p._id == post._id;
                    });

                    if (postIndex != -1) $scope.news.splice(postIndex,1);

                })
                .catch(function (err) {
                    $ionicLoading.show({
                        template: 'We are embarrassed, there was an error removing post.',
                        duration: 2000
                    });
                    errorServices.logError(err, "trying to remove post from wall");
                })
        }

        /**
         * Facebook Share.
         * @param post
         */
        function fbShare (post) {

            $translate(['titles.confirm', 'messages.confirm_publish']).then(function(trans){
                var confirmPopup = $ionicPopup.confirm({
                    title: trans['titles.confirm'],
                    template: trans['messages.confirm_publish']
                });

                confirmPopup.then(function(res) {
                    if(res) {
                        publish(post);
                    }
                });
            });

            function publish(post) {

                ngFB.login({scope: 'email, public_profile, publish_actions, user_friends'})
                    .then( function (response) {
                        if (response.status === 'connected') {
                            //Login success

                            ngFB.api({
                                method: 'POST',
                                path: '/v2.7/me/feed',
                                params: {
                                    caption: "This is happening in Class Buzz right now!",
                                    message: post.info.text,
                                    link: post.info.image
                                }})
                                .then( function (result) {
                                    var _msg = "Post shared";
                                    $cordovaToast.show(_msg, 'long', 'bottom');
                                })

                        } else {
                            //Login Fail
                        }
                    })
                    .catch(function (err) {
                        $ionicLoading.show({
                            template: 'We are embarrassed, there was an posting to facebook.',
                            duration: 2000
                        });
                        errorServices.logError(err, "trying to post to facebook");
                    })

            }
        }

    });

