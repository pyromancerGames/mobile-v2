angular.module('classbuzz.parents')

    .controller('ParentController', function ($http, $rootScope, $ionicListDelegate, $ionicLoading, $state,
                                                authenticationServices, userServices, $ionicModal, $socketio,
                                                $moment, $scope, $ionicPopup, messagesServices, errorServices) {


        $scope.children = [];
        $scope.moment = $moment;
        $scope.goTo = $state.go;

        populateChildren();

        function populateChildren() {
            userServices.getChildren()
            .then(function(result){
                $scope.children = result.data;
            })
        }

    });


