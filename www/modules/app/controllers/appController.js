angular.module('griffinmobile')


    .controller('AppController', function ($state, $http, $rootScope, $scope, $ionicModal, $ionicHistory, $socketio,
        $ionicLoading, $translate, $cordovaToast, _, coreServices, $ionicPopup, $ionicSideMenuDelegate, ionicDatePicker,
        authenticationServices, notificationServices, errorServices, userServices, customerServices, $moment) {

        // Form data for the login modal
        $scope.loginData = {};
        $scope.registerData = {};
        $scope.recoverData = {};
        $scope.registerData.parent1 = {};
        $scope.registerData.parent2 = {};
        $scope.userProfile = authenticationServices.getProfile();
        $scope.flashMessage = "";
        $scope.roleIdx = 1;
        $scope.showRoleSelection = true;
        $scope.modals = [];
        $scope.closeModal = closeModal;
        $scope.showModal = showModal;

        $scope.doRegister = doRegister;
        $scope.doRegisterParent = doRegisterParent;

        $scope.doLogout = doLogout;
        $scope.doLogin = doLogin;
        $scope.registerCodeIsValidAsync = registerCodeIsValidAsync;
        $scope.parentDNIIsValidAsync = parentDNIIsValidAsync;
        $scope.dniIsDuplicateAsync = dniIsDuplicateAsync;
        $scope.usernameIsDuplicateAsync = usernameIsDuplicateAsync;
        $scope.showTooltip = showTooltip;


        $scope.$on('$ionicView.enter', function(event){
            $scope.flashMessage = "";
        });

        $scope.openDatePicker = function() {

            var initData = {

                callback: function (val) {  //Mandatory
                    $scope.registerData.birthday = $moment(val);
                    $scope.registerData.humanBirthday = $moment(val).format("MMMM Do YYYY");
                },

                inputDate: new Date(),      //Optional
                mondayFirst: true,          //Optional
                closeOnSelect: false,       //Optional
                templateType: 'popup'       //Optional
            };

            ionicDatePicker.openDatePicker(initData);
        };

        function setUserLanguage() {
            userServices.getSettings()
            .then(function(result){
                $moment.locale(result.data.lang.code);
                $translate.use(result.data.lang.code);
            })
        }

        function showModal() {
            $scope.data = $scope.modals[0].data;
            $scope.modals[0].modal.show();
        }

        function closeModal() {
            $scope.modals[0].modal.hide();
            $scope.modals[0].modal.remove();

            if (typeof $scope.modals[0].cb == 'function') $scope.modals[0].cb();

            $scope.modals.shift();

            if ($scope.modals.length > 0) {
                showModal();
            }
        };

        if (authenticationServices.userIsLoggedIn()) {
            getFullUserProfile();
            setUserLanguage();
            initSocketIOEvents();
            initNotifications();
        } else {
            $state.go("app.home");
        }

        // Lang watcher
        $scope.updateLang = function(lngCode){
            $moment.locale(lngCode);
            $translate.use(lngCode);
        };

        // Triggered in the login modal to close it
        $scope.closeRegisterModal = function () {
            $scope.registerModal.hide();
            $scope.registerModal.remove();
            $rootScope.$broadcast('cb.hidetooltip');
        };

        $scope.closeForgotPasswordModal = function () {
            $scope.forgotPasswordModal.hide();
            $scope.forgotPasswordModal.remove();
            $rootScope.$broadcast('cb.hidetooltip');
        };

        $scope.hasRole = authenticationServices.hasRole;
        $scope.isLoggedIn = authenticationServices.userIsLoggedIn;

        // Open the register modal
        $scope.register = function () {

            $ionicModal.fromTemplateUrl('modules/app/views/register.html', { scope: $scope })
                .then(function (modal) {

                    $scope.registerData.centerCode = '';
                    $scope.registerData.dni = null;
                    $scope.registerData.firstName = '';
                    $scope.registerData.lastName = '';
                    $scope.registerData.email = '';
                    $scope.registerData.gender = 'M';
                    $scope.registerData.username = '';
                    $scope.registerData.password = '';


                    $scope.registerData.parent1.firstName = '';
                    $scope.registerData.parent1.lastName = '';
                    $scope.registerData.parent1.email = '';
                    $scope.registerData.parent1.dni = null;


                    $scope.registerData.parent2.firstName = '';
                    $scope.registerData.parent2.lastName = '';
                    $scope.registerData.parent2.email = '';
                    $scope.registerData.parent2.dni = null;

                    $scope.registerModal = modal;
                    $scope.registerModal.show();
                });
        };

        $scope.forgotPassword = function () {
            $ionicModal.fromTemplateUrl('modules/app/views/forgotPassword.html', { scope: $scope })
                .then(function (modal) {
                    $scope.forgotPasswordModal = modal;
                    $scope.forgotPasswordModal.show();
                });
        };

        //TODO Translate this.
        $scope.sendRecoverEmail = function () {

            if (typeof $scope.recoverData.email == 'undefined' || $scope.recoverData.email == null)  {
                $ionicLoading.show({ template: "El email no puede ser vacio.", duration:3000});
                return;
            }

            $ionicLoading.show({ template: "Sending email, please wait...<ion-spinner></ion-spinner>" });
            authenticationServices.recoverPassword($scope.recoverData.email)
            .then(function(result){
                $ionicLoading.hide();
                $ionicLoading.show({ template: "Mail sent: Check your inbox.", duration:3000});
                $scope.closeForgotPasswordModal();
            })
            .catch(function(err){
                $ionicLoading.hide();
                $ionicLoading.show({ template: err, duration:3000});
            })
        }

        //Prompt for logout confirmation
        $scope.logout = function () {

            $translate(["titles.confirm_logout", "messages.confirm_logout", "buttons.yes"])
                .then(function (translations) {

                    var confirmPopup = $ionicPopup.confirm({
                        title: translations["titles.confirm_logout"],
                        template: translations["messages.confirm_logout"]
                    });

                    confirmPopup.then(function (res) {
                        if (res) {
                            doLogout()
                        }
                    });

                });
        };

        $scope.goToNotifications = function () {
            $state.go("app.notifications");
            return;
        };


        function showTooltip(code) {

            $translate(code).then(function (translation) {

                var tooltip = {
                    text: translation,
                    btnText: "OK",
                    showBtn: true,
                    duration: 5000
                };

                $rootScope.$broadcast('cb.showtooltip', tooltip);
            });
        }

        function registerCodeIsValidAsync(code) {
            $scope.registerData.branch = {};
            return new Promise(function (resolve, reject) {
                coreServices.getBranchByCode(code)
                    .then(function (result) {
                        if (!_.isEmpty(result.data)) {
                            $scope.registerData.branch = result.data;
                            $scope.registerData.customer = result.data.customer;
                            resolve();
                        }
                        else reject()
                    })
            });
        }

        function dniIsDuplicateAsync(doc) {
            return new Promise(function (resolve, reject) {
                coreServices.dniExists(doc)
                    .then(function (result) {
                        if (result.data.exists) {
                            reject(result.data.exists);
                        }
                        else resolve(result.data.exists)
                    })
            });
        }

        function usernameIsDuplicateAsync(username) {
            return new Promise(function (resolve, reject) {
                coreServices.usernameExists(username)
                    .then(function (result) {
                        console.log(result);
                        if (result.data.exists) {
                            reject(result.data.exists);
                        }
                        else resolve(result.data.exists)
                    })
            });
        }

        //Check if parent isnt already registerd and if has any student linked to that document, otherwise cant register.
        function parentDNIIsValidAsync(doc) {

            return new Promise(function (resolve, reject) {

                dniIsDuplicateAsync(doc)
                    .then(function(result){
                        var params = {
                            dni: doc
                        };
                        return userServices.getChildrenParents(doc, params);
                    })
                    .then(function(result){
                        if (result.data != null)
                            resolve();
                        else
                            reject();
                    })
                    .catch(function(err) {
                        reject();
                    })
            });
        }

        function assignHouse() {

            var house;

            doLogin(false)
                .then(function (result) {
                    return customerServices.getNextSorting($scope.registerData.customer);
                })
                .then(function (result) {
                    house = result.data;
                    return userServices.addHouse(house._id);
                })
                .then(function (result) {

                    $scope.userProfile.houseLogo = house.logo;

                    $ionicModal.fromTemplateUrl('modules/app/views/houseSortingModal.html', {
                        scope: $scope

                    }).then(function (modal) {

                        var data = {
                            data: house,
                            modal: modal,
                            cb: endRegistration
                        };

                        $scope.modals.push(data);
                        if ($scope.modals.length == 1) showModal();
                    });
                })
        }

        function endRegistration(lang) {
            if(typeof lang == 'undefined' || lang == null) lang = 'en';

            $scope.registerModal.hide();
            $scope.registerModal.remove();

            //TODO Change to use user preferences lang.
            //$translate.use('en');
            $translate.use(lang);
            //setUserLanguaje();

            $state.go("app.profile", { "userId": $scope.userProfile.id });
        }

        function doRegister() {
            $ionicLoading.show({ template: "Creating user, please wait...<ion-spinner></ion-spinner>" });

            authenticationServices.registerUser($scope.registerData)
                .then(function (result) {
                    $scope.loginData.username = $scope.registerData.username;
                    $scope.loginData.password = $scope.registerData.password;
                    assignHouse();
                })
                .catch(function (error) { errorServices.logError(error, 'Registering student'); })
        }


        function doRegisterParent() {
            $ionicLoading.show({ template: "Creating user, please wait...<ion-spinner></ion-spinner>" });
            authenticationServices.registerParent($scope.registerData)
                .then(function (result) {
                    $scope.loginData.username = $scope.registerData.username;
                    $scope.loginData.password = $scope.registerData.password;
                    return doLogin(false);
                })
                .then(function(result){
                    endRegistration('es');
                })
                .catch(function (error) {
                    errorServices.logError(error, 'Registering parent');
                })
        }


        function doLogout() {
            authenticationServices.logOut();

            $ionicHistory.nextViewOptions({
                disableBack: true
            });

            resetVariables();
            closeAllDrawers();
            $ionicSideMenuDelegate.toggleRight();

            $socketio.emit("logout");

            $state.go("app.home");
            return;
        }

        function getFullUserProfile() {
            return userServices.getProfile()

                .then(function (result) {
                    $scope.userProfile = result.data;

                    //Assign avatar profile picture if different from null. Otherwise stay with placeholder
                    if ($scope.userProfile.avatarURL != null && $scope.userProfile.avatarURL != "") {
                        $scope.avatarURI = $scope.userProfile.avatarURL;
                    }
                    else {
                        //TODO put an actual image in img/
                        $scope.avatarURI = $rootScope.gfConfig.avatarPlaceholder;
                    }

                    if ($scope.userProfile.student.house) $scope.userProfile.houseLogo = $scope.userProfile.student.house.logo;
                    //else $scope.userProfile.houseLogo = $rootScope.gfConfig.baseURL + $rootScope.gfConfig.avatarPlaceholder;

                    $ionicLoading.hide();
                    $scope.$broadcast('scroll.refreshComplete');

                    return result.data;
                })
                .catch(function (err) {
                    $translate("messages.embarrassing_error").then(function (errorText) {
                        $ionicLoading.hide();
                        errorText = '<p>' + errorText + '</p>';
                        $ionicLoading.show({ template: errorText, duration: 2000 });
                        errorServices.logError(err, "fetching full user profile");
                    });
                })
        }


        function doLogin(redirect) {

            if (typeof redirect == 'undefined') redirect = true;

            $ionicLoading.show({ template: "Logging in, please wait...<ion-spinner></ion-spinner>" });

            return authenticationServices.logIn($scope.loginData)
                .then(function (response) {

                    if (response.data.token) {

                        // persist user
                        authenticationServices.setToken(response.data.token);
                        authenticationServices.setProfile(response.data.token);

                        $ionicHistory.nextViewOptions({
                            disableBack: true
                        });

                        $ionicHistory.clearCache();
                        $ionicHistory.clearHistory();

                        $scope.loginData = {}; //Reset loginData
                        $scope.userProfile = authenticationServices.userProfile;
                        
                        setUserLanguage();
                        getFullUserProfile();
                        initSocketIOEvents();
                        initNotifications();

                        $rootScope.$broadcast('cb.userloaded');
                        $ionicLoading.hide();

                        if (redirect)
                            $state.go("app.profile", { "userId": $scope.userProfile.id });
                    }

                }).catch(function (err) {
                    $ionicLoading.hide();
                    $scope.flashMessage = err.data.reason;
                    if(err.data.status == 569) {
                        // Comparing hashes error - Reset password
                        // display popup to ask teacher to reset password or follow the forgot password process
                        $ionicLoading.show({ template: 'There was an error with your password, please ask your teacher to restore it or click below on the "Forgot Password?" link and complete the process', duration:10000});
                    }
                });
        }


        $scope.isEmpty = function (obj) {
            return _.isEmpty(obj);
        };


        function resetVariables() {
            $scope.pendingNotifications = 0;
        }

        function closeAllDrawers () {
            $rootScope.$broadcast('cb.hidetooltip');
        }

        function initNotifications() {
            $scope.pendingNotifications = 0;

            notificationServices.getPendingNotifications()
                .then(function (notifications) {

                    notifications.data.forEach(function (n) {
                        if (!n.isRead) $scope.pendingNotifications += 1;
                    });

                })
                .catch(function (err) { errorServices.logError(err, "trying to init notifications"); })
        }

        function initSocketIOEvents() {
            console.info("Initializing socket.io Events");
            $socketio.emit("tieandjoin", authenticationServices.userProfile.id, authenticationServices.userProfile.customer);

            $socketio.on('newnotification', function (notification) {

                $scope.pendingNotifications += 1;
                var _msg = "New Notification";

                //To prevent error in browser
                if (ionic.Platform.platforms[0] != 'browser') $cordovaToast.show(notification.title, 'long', 'bottom');

                //Do Cool Stuff
                console.log(_msg + ': Change this for cool stuff!!');

                $rootScope.$emit('newnotification', notification);
            });


            $socketio.on('message.new', function (msg) {
                if (msg.from._id.toString() == authenticationServices.userProfile.id.toString()) return;

                // if ($ionicHistory.currentStateName() != 'app.messages' || $ionicHistory.currentStateName() != 'app.messagesDetail') {
                //     var tooltip = {
                //         title: "New Message",
                //         text: msg.from.firstName + ' wrote: ' + msg.body,
                //         btnText: "Close"
                //     };

                //     $rootScope.$broadcast('cb.showtooltip', tooltip);
                // }

            });

            $socketio.on('newquestavailable', function (quest) {

                var tooltip = {
                    title: "New Quest",
                    text: quest.name,
                    btnText: "Awesome"
                };

                $rootScope.$broadcast('cb.showtooltip', tooltip);
                $rootScope.$emit('newquestavailable', quest);
            });

            $socketio.on('questcompleted', function (quest) {

                $ionicModal.fromTemplateUrl('modules/app/views/questCompletedModal.html', {
                    scope: $scope
                }).then(function (modal) {

                    var data = {
                        data: {
                            quest: quest
                        },
                        modal: modal
                    }

                    $scope.modals.push(data);
                    if ($scope.modals.length == 1) showModal();
                });


                $scope.userProfile.student.character.money += quest.reward.money;
                $scope.userProfile.student.character.levelInfo.xp += quest.reward.xp;

                $rootScope.$emit('questcompleted', quest);
            });

            $socketio.on('activitycompleted', function (activity) {
                //Show Toast
                var _msg = "Activity Completed!";

                //To prevent error in browser
                if (ionic.Platform.platforms[0] != 'browser') $cordovaToast.show(_msg, 'long', 'bottom');

                //Do Cool Stuff
                $scope.userProfile.student.character.money += activity.reward.money;
                $scope.userProfile.student.character.levelInfo.xp += activity.reward.xp;

                $rootScope.$emit('activitycompleted', activity);
            });

            $socketio.on('traitacquired', function (trait) {
                //Show Toast
                var _msg = "Trait Acquired!";

                //To prevent error in browser
                if (ionic.Platform.platforms[0] != 'browser') $cordovaToast.show(_msg, 'long', 'bottom');

                //Do Cool Stuff
                console.log(_msg + ': Change this for cool stuff!!');

                $rootScope.$emit('traitacquired', trait);
            });

            $socketio.on('skilllevelup', function (skill, newRank) {

                $ionicModal.fromTemplateUrl('modules/app/views/rankLevelUpModal.html', {
                    scope: $scope

                }).then(function (modal) {

                    var data = {
                        data: {
                            skill: skill,
                            rank: newRank
                        },
                        modal: modal
                    }

                    $scope.modals.push(data);
                    if ($scope.modals.length == 1) showModal();
                });

                $rootScope.$emit('skilllevelup', skill, newRank);
            });

            $socketio.on('achievementunlocked', function (achievement) {

                $ionicModal.fromTemplateUrl('modules/app/views/achievementUnlockedModal.html', {
                    scope: $scope

                }).then(function (modal) {

                    var data = {
                        data: achievement,
                        modal: modal
                    };

                    $scope.modals.push(data);
                    if ($scope.modals.length == 1) showModal();
                });

                $rootScope.$emit('achievementunlocked', achievement);
            });

        }

        $rootScope.$on('newnotification', function () {
            $scope.pendingNotifications += 1;
        });

        $rootScope.$on('notificationdeleted', function () {
            $scope.pendingNotifications -= 1;
        });

        $rootScope.$on('resetnotificationcount', function () {
            $scope.pendingNotifications = 0;
        });

        $rootScope.$on('notificationread', function () {
            $scope.pendingNotifications -= 1;
        })

    });

