(function () {

    angular.module('classbuzz.store')

        .factory('storeServices', function ($http, $rootScope, authenticationServices, apiServices, userServices) {

            var services = {

                /**
                 * Get traits for a top menu, lazy loading
                 * @returns {*}
                 */
                getTraits: function(searchText, filter, limit, page) {

                    // Default values
                    limit = limit || 5;
                    page = page || 1;

                    var data = {
                        "searchField" : searchText,
                        "limit" : limit,
                        "page": page,
                        "searchFilters": filter
                    };

                    var endpoint = '/traits/paginate';
                    return apiServices.POST(endpoint, data);
                },

                updateCharEquippedTraits: function(char, equippedTraits) {

                    // Set new values
                    var newChar = JSON.parse(JSON.stringify(char));

                    newChar.traits.body = services.parseBodyIds(equippedTraits.body);
                    newChar.traits.wearables = services.parseWearablesIds(equippedTraits.wearables);

                    newChar.traits.companion = null;
                    newChar.inventory = char.inventory;

                    return userServices.updateChar(newChar, authenticationServices.getUserDoc())
                },

                parseBodyIds: function(body) {

                    var traitsBody = JSON.parse(JSON.stringify(body));

                    for (var key in traitsBody) {
                        if(traitsBody[key]) {
                            traitsBody[key] = traitsBody[key]._id;
                        }
                    }

                    return traitsBody;
                },

                parseWearablesIds: function (wearables) {

                    var traitsWearables = JSON.parse(JSON.stringify(wearables));

                    for (var key in traitsWearables) {
                        if(traitsWearables[key]) {
                            traitsWearables[key] = traitsWearables[key]._id;
                        }
                    }

                    return traitsWearables;
                },

                getDefaultTrait: function (gender) {
                    var endpoint = '/traits/default/find/gender/' + gender;
                    return apiServices.GET(endpoint);
                },

                getSingleTrait: function(traitName) {
                    var data= {
                        name: traitName
                    }
                    var endpoint = '/traits/query';
                    return apiServices.POST(endpoint, data);
                },

                charCanBuyTrait: function(isLoading, trait, userStudent, character) {
                    var canBuyTrait = true;

                    if(!isLoading) {

                        if(userStudent){

                            // Check user doublouns
                            if(trait.price > userStudent.currency.doubloons) {
                                canBuyTrait = false;
                            }

                            // Check level req
                            //if(canBuyTrait && trait.requirements.level > viewModel.character.levelInfo.level) {
                            if(canBuyTrait && trait.requirements.level > character.levelInfo.level) {
                                canBuyTrait = false;
                            }

                            // Check trait req
                            if(canBuyTrait && trait.requirements.trait) {
                                canBuyTrait = services.checkForRequieredTrait(trait, character);
                            }
                        }

                        return canBuyTrait;

                    } else {
                        return false;
                    }
                },


                checkForRequieredTrait: function (trait, character) {

                    var hasReq = false;

                    character.inventory.forEach(function(element) {

                        var rq = trait._id == element;

                        if(rq) {
                            // Exists inside charInventory
                            hasReq = true;
                            return;
                        }

                    }, this);

                    return hasReq;
                }

            };
            return services;
        });

})();
