angular.module('classbuzz.settings')

    .controller('SettingsController', function ($rootScope, $ionicLoading, $translate, $ionicModal, userServices,
                                                $moment, $scope, errorServices) {


        $scope.moment = $moment;
        $scope.availableLangs = [ { code: "en", dsc: "English" }, { code: "es", dsc: "Español" } ];
        $scope.passwordData = {
            password1: "",
            password2: ""
        };

        $scope.availableTZ = [{ code: "America/Montevideo", dsc: "America/Montevideo"}];
        $scope.selectedLang = {};
        $scope.selectedTZ = {};

        // $scope.availableTZ = [];
        // $moment.tz.names().forEach(function(tz){ $scope.availableTZ.push({ code: tz, dsc: tz}) })

        $scope.applySettings = applySettings;
        $scope.setDefaults = setDefaults;
        $scope.changePassword = changePassword;
        $scope.closeModal = closeModal;
        $scope.doChangePassword = doChangePassword;
        $scope.passwordsDontMatch = false;

        populateUserSettings();

        $scope.changeLang = function(lang) {
            $scope.selectedLang = lang;
        }

        function setDefaults () {
            $scope.selectedLang = { code: "en", dsc: "English"};
            $scope.selectedTZ = {code:"America/Montevideo", dsc:"America/Montevideo"};
        }

        function applySettings() {

            console.log($scope.selectedLang);

            $translate("messages.updating_settings").then(function(fetchingText){
                fetchingText = '<p>'+fetchingText+'</p><ion-spinner></ion-spinner>';
                $ionicLoading.show({template:fetchingText});
            });

            var settings = {
                lang: {
                    code: $scope.selectedLang.code,
                    dsc: $scope.selectedLang.dsc
                },
                timezone: $scope.selectedTZ.code
            };

            console.log(settings);


            userServices.updateSettings(settings)
                .then(function(result){
                    $ionicLoading.hide();
                    $moment().locale($scope.selectedLang.code);
                    $translate.use($scope.selectedLang.code);

                    $translate("messages.saved").then(function(fetchingText){
                        $ionicLoading.show({template:fetchingText, duration:1000});
                    });
                })
                .catch(function(err){
                    $translate("messages.embarrassing_error").then(function(fetchingText){
                        fetchingText = '<p>'+fetchingText+'</p><ion-spinner></ion-spinner>';
                        $ionicLoading.show({template:fetchingText, duration:3000});
                    });
                })

        }

        function populateUserSettings () {
            userServices.getSettings()
                .then(function(result){

                    if (result.data == null || result.data == {}) {
                        $scope.selectedLang = { code: "en", dsc: "English"};
                        $scope.selectedTZ = {code:"America/Montevideo", dsc:"America/Montevideo"};
                    }
                    else {
                        $scope.selectedTZ = {code: result.data.timezone, dsc: result.data.timezone};
                        $scope.selectedLang = result.data.lang;
                    }

                })
                .catch(function(err){
                    $translate("messages.embarrassing_error").then(function(fetchingText){
                        fetchingText = '<p>'+fetchingText+'</p><ion-spinner></ion-spinner>';
                        $ionicLoading.show({template:fetchingText, duration:3000});
                    });
                })
        }

        function doChangePassword(){
            if ($scope.passwordData.password1 != $scope.passwordData.password2) {
                $scope.passwordsDontMatch = true;
                return;
            }

            $scope.passwordsDontMatch = false;

            $translate("messages.updating_password").then(function(fetchingText){
                fetchingText = '<p>'+fetchingText+'</p><ion-spinner></ion-spinner>';
                $ionicLoading.show({template:fetchingText});
            });

            userServices.updatePassword($scope.passwordData.password2)
            .then( function(result){
                $ionicLoading.hide();
                $scope.closeModal();
                $translate("messages.saved").then(function(fetchingText){
                    $ionicLoading.show({template:fetchingText, duration:1000});
                });
                $scope.passwordData = { password1: "", password2: "" };
            })
            .catch(function(err){
                $translate("messages.embarrassing_error").then(function(fetchingText){
                    fetchingText = '<p>'+fetchingText+'</p><ion-spinner></ion-spinner>';
                    $ionicLoading.show({template:fetchingText, duration:3000});
                });
            })
        }

        function changePassword () {
            $ionicModal.fromTemplateUrl('modules/settings/views/changePassModal.html', { scope: $scope })
                .then(function (modal) {
                    $scope.changePasswordModal = modal;
                    $scope.changePasswordModal.show();
                });
        };

        function closeModal() {
            $scope.changePasswordModal.hide();
            $scope.changePasswordModal.remove();
            $rootScope.$broadcast('cb.hidetooltip');
        };


    });


