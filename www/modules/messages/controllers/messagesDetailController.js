angular.module('classbuzz.messages')

    .controller('MessagesDetailController', function ($http, $rootScope, $ionicListDelegate, $ionicLoading, $state, _,
                                                      $stateParams, $socketio, userServices, $timeout, $ionicModal, $translate,
                                                      $moment, $scope, $ionicPopup, messagesServices, $ionicHistory,
                                                      authenticationServices, errorServices) {

        $scope.conversation = [];
        $scope.newMsg = "";
        $scope.send = send;
        $scope.interlocutor = {};
        $scope.interlocutorStatus = "";
        $scope.messageToEdit = {};
        var isWriting = false;
        var socketTask = null;

        getConversation($stateParams.user1, $stateParams.user2);

        if ($stateParams.user2 == authenticationServices.userProfile.id)
            getInterlocutor($stateParams.user1);
        else
            getInterlocutor($stateParams.user2);

        $scope.processWriting = processWriting;
        $scope.hasRole = authenticationServices.hasRole;

        // Create the selectStudents modal
        $ionicModal.fromTemplateUrl('modules/messages/views/editMessageModal.html', {scope: $scope})
        .then(function (modal) {
            $scope.editMessageModal = modal;
        });

        $socketio.emit("user.get.status", $stateParams.user2);

        $scope.fromNow = function (date) {
            return ($moment(date).fromNow());
        };

        $scope.editMessage = function(message) {
            if (message.dateSent != null) return;
            if (!$scope.messageIsOwn(message)) return;

            $scope.messageToEdit = message;
            $scope.editMessageModal.show();
        }
    
        $scope.trashMessage = function(message) {
            $translate(["titles.confirm_delete", "messages.confirm_delete", "buttons.yes"])
                .then(function (translations) {

                    var confirmPopup = $ionicPopup.confirm({
                        title: translations["titles.confirm_delete"],
                        template: translations["messages.confirm_delete"]
                    });

                    confirmPopup.then(function (res) {
                        if (res) {
                            messagesServices.remove(message)
                            .then(function(result){
                                var mIdx = _.findIndex($scope.conversation, function(m){
                                    return m._id.toString() == message._id.toString();
                                })

                                if (mIdx != -1) {
                                    $scope.conversation.splice(mIdx,1);
                                }

                            })
                            .catch(function(err){
                                $ionicLoading.show({template: err, duration:2000});
                            })
                        }
                    });

                });
        }

        $scope.closeModal = function () {
            $scope.editMessageModal.hide();
            $scope.messageToEdit = {};   
        }

        $scope.doEditMessage = function () {
            messagesServices.edit($scope.messageToEdit)
            .then(function(result){
                $scope.editMessageModal.hide();
            })
            .catch(function(err){
                $ionicLoading.show({template: err, duration:2000});
            })
        }

        $scope.humanizeDate = function (date) {
            return ($moment(date).format('DD MMMM, YYYY'))
        }

        $scope.goBack = function(){
            $ionicHistory.goBack();
        }

        $scope.messageIsOwn = function(msg) {
            if (!authenticationServices.userIsLoggedIn()) return false;
            return msg.from._id.toString() == authenticationServices.userProfile.id.toString();
        }

        function getConversation (user1, user2) {
            messagesServices.getConversation(user1, user2)
                .then(function(result){
                    $scope.conversation = result.data;
                })
        }

        $socketio.on('message.new', function (msg) {
            $scope.conversation.push(msg);
        });

        // $socketio.on('user.status', function (user, status) {
            
        //     if (user.toString() != $stateParams.user2.toString()) return;

        //     if (status.status == 'online')
        //         $scope.interlocutorStatus = 'online';
        //     else {
        //         if (status.last == 'never')
        //             $scope.interlocutorStatus = 'last: never';
        //         else
        //             $scope.interlocutorStatus = 'last: ' + $scope.fromNow(status.last);
        //     }

        // });

        // $socketio.on('message.writing', function (user) {
        //     if (user.toString() == $stateParams.user2.toString()) $scope.interlocutorStatus = 'is writing...';
        // });

        // $socketio.on('message.stopped.writing', function (user) {
        //     if (user.toString() == $stateParams.user2.toString()) $scope.interlocutorStatus = 'Online';
        // });

        function processWriting () {

            if (socketTask) {
                $timeout.cancel(socketTask);

                socketTask = $timeout(function(){
                    isWriting = false;
                    $socketio.emit("message.stopped.writing", authenticationServices.userProfile.id, $stateParams.user2);
                }, 3000)
            }

            if (!isWriting) {
                $socketio.emit("message.writing", authenticationServices.userProfile.id, $stateParams.user2);
                isWriting = true;

                socketTask = $timeout(function(){
                    isWriting = false;
                    $socketio.emit("message.stopped.writing", authenticationServices.userProfile.id, $stateParams.user2);
                }, 2000)
            }
        }


        function send (msg) {
            if (msg == "" || msg == null) return;

            $socketio.emit("message.new", authenticationServices.userProfile.id, $stateParams.user2, msg);
            this.newMsg = null;

            $timeout.cancel(socketTask);
            isWriting = false;
            $socketio.emit("message.stopped.writing", authenticationServices.userProfile.id, $stateParams.user2);
        }

        function getInterlocutor(userId) {
            userServices.getProfile(userId)
                .then(function(result){
                    $scope.interlocutor = result.data;            
                })
        }

    });



