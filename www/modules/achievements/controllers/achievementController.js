angular.module('classbuzz.achievements')

    .controller('AchievementController', function ($http, $rootScope, $scope, $socketio,
                                              $ionicModal, $ionicListDelegate, authenticationServices,
                                              errorServices, achievementServices, cbUtils, userServices, _) {


        $scope.userAchievements = {};
        $scope.customerAchievements = {};
        $scope.sortedUserAchievements = [];

        $scope.doRefresh = doRefresh;
        $scope.countUserAchievements = countUserAchievements;
        $scope.lastTimestampOfId = lastTimestampOfId;
        $scope.fromNow = cbUtils.fromNow;
        $scope.userProfile = authenticationServices.userProfile;


        populateAchievements();

        function doRefresh () {

        }

        function populateAchievements() {

            userServices.getAchievements()
                .then(function (achievements) {
                    $scope.userAchievements.data = achievements.data;

                    var uniqueList = _.uniq($scope.userAchievements.data, function(item){
                        return item.achievement;
                    });

                    uniqueList = _.filter(uniqueList, function(a){
                        return a.achievement.type == 'reward';
                    });

                    var idCount = _.countBy(uniqueList, function(item){
                        return item.achievement._id;
                    });

                    $scope.userAchievements.idRecount = idCount;
                    $scope.distinctAchievementsReceived = Object.keys(idCount).length;
                    $scope.sortedUserAchievements =_.sortBy($scope.userAchievements.data, function(a){
                        return a.timestamp;
                    }).reverse();

                    return achievementServices.getCustomerAchievements();
                })
                .then(function (achievements) {

                    $scope.customerAchievements.data = _.sortBy(achievements.data, function(a){
                        return countUserAchievements(a._id);
                    }).slice().reverse();

                    var typeCount = _.countBy($scope.customerAchievements.data, function(item){
                        return item.type;
                    });

                    var idCount = _.countBy($scope.customerAchievements.data, function(item){
                        return item._id;
                    });


                    $scope.customerAchievements.typeRecount = typeCount;
                    $scope.customerAchievements.idRecount = idCount;

                })
                .catch(function (err) {
                    errorServices.logError(err, "Doing achievement processing stuff!");
                })
        };


        function countUserAchievements(achievementId){

            if (achievementId in $scope.userAchievements.idRecount)
                return $scope.userAchievements.idRecount[achievementId];
            else
                return 0;
        };


        function lastTimestampOfId(achievementId){
            for (var i=0; i < $scope.sortedUserAchievements.length; i++) {
                if ($scope.sortedUserAchievements[i].achievement._id == achievementId) {
                    return $scope.sortedUserAchievements[i].timestamp;
                }
            };
        };

    })

