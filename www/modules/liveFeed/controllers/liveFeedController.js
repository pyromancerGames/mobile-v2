angular.module('classbuzz.liveFeed')

    .controller('LiveFeedController', function ($http, $rootScope, $scope,
                                                $socketio, $moment, $ionicLoading, $state,
                                                authenticationServices,
                                                errorServices, eventServices) {
        $scope.feeds = [];
        $scope.loggedUser = authenticationServices.userProfile;
        $scope.moreItemsAvailable = true;
        $scope.loadMore = loadMore;

        var nextItems = [];


        $scope.$on('$ionicView.enter', function () {
            if(!authenticationServices.userIsLoggedIn()) {
                $state.go("app.home");
                return;
            }
        });

        $scope.doRefresh = doRefresh;

        $socketio.on('newevent', function(event) {
            $scope.feeds.unshift(event);
        });

        $scope.fromNow = function(date){
          return($moment(date).fromNow());
        };

        function doRefresh(){
            $scope.feeds = [];
            $scope.moreItemsAvailable = true;
            nextItems = [];
            preloadNext().then(loadMore());
        }

        function preloadNext() {
            $scope.moreItemsAvailable = false;

            return eventServices.getLazyEvents($scope.feeds.length)
                .then(function (result) {
                    $scope.$broadcast('scroll.refreshComplete'); // Stop the ion-refresher from spinning
                    $ionicLoading.hide();

                    if (result.data.length  == 0) {
                        $scope.moreItemsAvailable = false;
                    }
                    else {
                        $scope.moreItemsAvailable = true;
                        nextItems = nextItems.concat(result.data);
                    }

                })
                .catch(function (err) {
                    $scope.$broadcast('scroll.refreshComplete'); // Stop the ion-refresher from spinning
                    $ionicLoading.hide();
                    $ionicLoading.show({
                        template: 'We are embarrassed, there was an error while preloading data for live feed. Please contact support',
                        duration: 2000
                    });
                    errorServices.logError(err, "trying to preload data for live feed");
                })
        }


        function loadMore() {
            if (nextItems.length > 0) $scope.feeds.push(nextItems.shift());
            if (nextItems.length == 0) preloadNext();
            $scope.$broadcast('scroll.infiniteScrollComplete');
        }

    })

