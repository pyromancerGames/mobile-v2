(function () {
    angular.module('classbuzz.groups')
        .factory('groupServices', function ($rootScope, $localStorage, $location, authenticationServices, _, apiServices) {

            var services = {

                /**
                 * Get a group
                 * @param _data
                 * @returns {*}
                 */
                getGroup: function (_id) {
                    var endpoint = '/groups/' + _id;
                    return apiServices.GET(endpoint);
                },

                /**
                 * Updates a group
                 * @param _data
                 * @returns {*}
                 */
                updateGroup: function (_data) {
                    var endpoint = '/groups';
                    return apiServices.PUT(endpoint,_data);
                },

                /**
                 * Removes a group
                 * @param groupId
                 * @returns {*}
                 */
                removeGroup: function (groupId) {
                    var endpoint = '/groups';
                    return apiServices.DELETE(endpoint, {_id: groupId} );
                },

                /**
                 * Each customer has a set of levels that can be assigned to its groups.
                 * This endpoint fetchs that levels
                 * @param customerId
                 * @returns {*}
                 */
                getGroupLevels: function (customerId) {
                    //TODO this should be a customer endpoint not groups.
                    var endpoint = '/groups/customer/' + customerId + '/levels';
                    return apiServices.GET(endpoint);
                },

                /**
                 * Get all students for a given group.
                 * @param groupId
                 * @returns {*}
                 */
                getGroupStudents: function(groupId) {
                    var endpoint = '/groups/' + groupId + '/students';
                    return apiServices.GET(endpoint);
                },

                /**
                 * Get all Groups for a given teacher
                 * @param userId
                 * @returns {*}
                 */
                getTeacherGroups: function(userId, params) {
                    if (typeof userId == "undefined" || userId == null) userId = authenticationServices.userProfile.id;
                    var endpoint = '/groups/teacher/' + userId;
                    return apiServices.GET(endpoint, params);
                },


                createGroup: function(groupData) {
                    if (typeof groupData.teacher == "undefined" || groupData.teacher == null) groupData.teacher = authenticationServices.userProfile.id;
                    if (typeof groupData.branch == "undefined" || groupData.branch == null) groupData.branch = authenticationServices.userProfile.branch;
                    var endpoint = '/groups';
                    return apiServices.POST(endpoint, groupData);
                },


                addToGroup: function(groupId, studentId) {

                    var _data = {
                        studentId: studentId
                    };

                    var endpoint = '/groups/' + groupId + '/student';
                    return apiServices.POST(endpoint, _data);
                },

                removeFromGroup: function(groupId, studentId) {
                    var _data = {
                        studentId: studentId
                    };

                    var endpoint = '/groups/' + groupId + '/student';
                    return apiServices.DELETE(endpoint, _data);
                },


                getGroupComplement: function(groupId, searchField, searchLimit){
                    if (typeof searchField == "undefined" || searchField == null) searchField = "";
                    if (typeof searchLimit == "undefined" || searchLimit == null) searchLimit = 10;

                    var _data = {
                        searchField: searchField,
                        searchLimit: searchLimit
                    };

                    var endpoint = '/groups/' + groupId + '/complement';
                    return apiServices.POST(endpoint, _data);
                }

            };
            return services;
        });

})();

