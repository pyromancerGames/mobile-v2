(function () {

    angular.module('classbuzz.messages')

        .factory('messagesServices', function ($http, $rootScope, authenticationServices, apiServices) {

            var services = {

                getConversation: function (user1, user2) {

                    var params = {
                        user1: user1,
                        user2: user2
                    };

                    var _endpoint = '/message/conversation';
                    return apiServices.GET(_endpoint, params);
                },


                getMessageboard: function (userId, params) {

                    if (typeof userId == "undefined" || userId == null) userId = authenticationServices.userProfile.id;

                    var _endpoint = '/message/board/' + userId;
                    return apiServices.GET(_endpoint, params);
                },

                enqueue: function(message) {
                    var data = message;
                    var endpoint = '/message/enqueue';
                    return apiServices.POST(endpoint, data);
                },

                edit: function(message) {
                    var data = message;
                    var endpoint = '/message';
                    return apiServices.PUT(endpoint, data);
                },

                remove: function(message) {
                    var data = {};
                    var endpoint = '/message/'+ message._id;
                    return apiServices.DELETE(endpoint, data);
                }

            };
            return services;
        });

})();
