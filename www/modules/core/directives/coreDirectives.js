(function() {
    angular.module('classbuzz.core')
        .directive('cbTooltip', function(){

            var controller = ['$scope', '$timeout', function($scope, $timeout) {

                $scope.showTooltip = false;
                $scope.tooltip = "";
                $scope.btnText = "";


                var tOut;

                $scope.$on('cb.showtooltip', function(event, tooltip) {

                    if (typeof tooltip.showBtn == 'undefined') tooltip.showBtn = true;

                    $scope.tooltip = tooltip.text;
                    $scope.title = tooltip.title || false;
                    $scope.btnText = tooltip.btnText;
                    $scope.showTooltip = true;
                    $scope.showBtn = tooltip.showBtn;

                    if (tooltip.duration) {
                        tOut = $timeout(function(){
                            hideTooltip();
                        }, tooltip.duration)
                    }
                });

                $scope.$on('cb.hidetooltip', function (){
                    hideTooltip();
                });

                $scope.hideTooltip = hideTooltip;

                function hideTooltip() {
                    $timeout.cancel(tOut);
                    isOut = false;
                    $scope.showTooltip = false;
                }

            }];

            var link = function(scope, element, attrs) {

                scope.$on('cb.showtooltip', function(event, tooltip) {
                    angular.element(element).removeClass( 'cloak' );
                });

            };


            var template = '<div id="cbtooltip" class="tooltip animated cloak" ng-class="showTooltip ? \'slideInUp\' : \'slideOutDown\'" ng-show="showTooltip">' +
                '<i class="ion-information-circled"></i>' +
                '<div class="title" ng-if="title">{{title}}</div>' +
                '<div class="text">{{tooltip}}</div>' +
                '<button class="cb-button" ng-click="hideTooltip()" ng-show="showBtn">{{btnText}}</button>' +
                '</div>';

            return {
                restrict: 'EA',
                replace: 'true',
                scope: {},
                template: template,
                controller: controller,
                link: link
            };

        })


        .directive('specialEventBadge', function(){

            var controller = ['$scope', '$rootScope', '$timeout','userServices', 'authenticationServices', function($scope, $rootScope, $timeout, userServices, authenticationServices) {

                $scope.nextSEvent = null;
                $scope.showRibbon = false;
                $scope.hideRibbon = false;

                var userLoaded = false;

                $scope.doHideRibbon = function(){
                    $scope.showRibbon = false;
                    $scope.hideRibbon = true;
                    $rootScope.$broadcast('cb.hideribbon');
                };

                $scope.doShowRibbon = function(){
                    $scope.hideRibbon = false;
                    $scope.showRibbon = true;
                }


                function getNextSpecialEvent () {
                    // Disable special events for parents
                    if(authenticationServices.hasRole('parent')) return;

                    userServices.getActiveSpecialEvents()
                        .then(function(result){
                            if (result.data && result.data.length) {
                                $scope.nextSEvent = result.data[0];
                                $rootScope.$broadcast('cb.showribbon');
                                $scope.doShowRibbon()
                            }
                        })
                }

                $scope.$on("cb.userloaded", function(event, tooltip) {
                    userLoaded = true;
                    getNextSpecialEvent();
                });

                $scope.$on("$ionicView.enter", function(event, tooltip) {
                    if (userLoaded) getNextSpecialEvent();
                });



            }];

            var template = '<div id="seRibbon" class="se-ribbon {{nextSEvent.difficulty}} cloak animated" ng-show="showRibbon" ng-click="doHideRibbon()" ' +
                'ng-class="{ \'slideOutUp\': hideRibbon, \'slideInDown\': showRibbon }">' +
                    '<div class="name">{{nextSEvent.name}}</div>' +
                '</div>';


            var link = function(scope, element, attrs) {

                scope.$on('cb.showribbon', function(event, tooltip) {
                    angular.element(element).removeClass( 'cloak' );
                });

            };

            return {
                restrict: 'EA',
                replace: 'true',
                scope: {},
                template: template,
                controller: controller,
                link: link
            };

        })


        .directive('cbProgressBar', function(){

            return {
                restrict: 'A',
                scope: {
                    width: '@',
                    height: '@',
                    pj: '@',
                    bgColor: '@',
                    barColor: '@',
                    max: '@',
                    current: '@'
                },
                replace: 'true',
                template:
                '<div>' +
                '<style> ' +

                '#progress-bar { ' +
                'width: {{width}}; ' +
                'height: {{height}}; ' +
                'background-color: {{bgColor}}; ' +
                '-moz-border-radius: 25px; ' +
                '-webkit-border-radius: 25px; ' +
                'border-radius: 25px;' +
                '}' +

                '#progress-bar #progress { ' +
                'display: block; ' +
                '-moz-border-radius: 25px; ' +
                '-webkit-border-radius: 25px; ' +
                'border-radius: 25px;' +
                'box-shadow: inset 0 1px 9px  rgba(255,255,255,0.3), inset 0 -1px 6px rgba(0,0,0,0.4); ' +
                'position: relative; ' +
                'overflow: hidden;' +
                'width: 50%; ' +
                'height: 100%; ' +
                'background-color: {{barColor}}; ' +
                'transition: width 1s linear; ' +
                '}' +

                '#progress-bar #caption { ' +
                'position: absolute; ' +
                'top: 50%; ' +
                'left: 50%; ' +
                'transform: translate(-50%, -50%); ' +
                'text-align:center;' +
                'color: rgb(255,255,255);' +
                'font-weight: 500;' +
                'font-size: 9px;' +
                '}' +

                '</style>' +
                '<div id="progress-bar">' +
                '<div id="progress"> ' +
                '<div id="caption"> {{caption}} </div>' +
                '</div>' +
                '</div> ' +
                '</div>',

                link: function(scope, element, attrs) {

                    var $wrapper = angular.element(element[0].querySelector("#progress-bar"));
                    var $progress = angular.element(element[0].querySelector("#progress-bar #progress"));
                    var $caption = angular.element(element[0].querySelector("#progress-bar #progress #caption"));


                    if (typeof scope.width != 'undefined') {
                        $wrapper.css('width', scope.width);
                    }


                    if (typeof scope.height != 'undefined') {
                        $wrapper.css('height', scope.height);
                    }

                    if (typeof scope.bgColor != 'undefined') {
                        $wrapper.css('background-color', scope.bgColor);
                    }

                    scope.$watch('current', function(newVal, oldVal, scope){
                        redraw()
                    });

                    scope.$watch('max', function(newVal, oldVal, scope){
                        redraw()
                    });

                    redraw();

                    function redraw() {

                        if (typeof scope.max != 'undefined' && typeof scope.current != 'undefined') {
                            var pj = scope.current * 100 / scope.max;
                            $progress.css('width', pj+'%');
                            $caption.text(Math.round(pj)+"%");
                        }
                    }

                    if (typeof scope.barColor != 'undefined') {
                        $progress.css('background-color', scope.barColor);
                    }

                }

            };

        })

        .directive('focusMe', function($timeout) {
            return {
                link: function(scope, element, attrs) {
                    scope.$watch(attrs.focusMe, function(value) {
                        if(value === true) {
                            $timeout(function() {
                                element[0].focus();
                                element[0].select();
                            });
                        }
                    });
                }
            };
        })


        .directive('cbSelect', function(){

            var template = '' +
                '<div class="select">' +
                    '<div class="select-styled">' +
                    '</div>' +
                    '<ul class="select-options"> ' +
                    '</ul>' +
                '</div>';


            var link = function(scope, element, attrs) {

                var $styledSelect = angular.element(element[0].querySelector('.select-styled'));
                var $selectOptions = angular.element(element[0].querySelector('ul'));

                $styledSelect.text(scope.cbPlaceholder);
                $styledSelect.removeClass('active');

                scope.$watch('cbModel', function(){
                    if (typeof scope.cbModel == "undefined" || scope.cbModel == {} || scope.cbModel == null) return;
                    $styledSelect.text(scope.cbModel[scope.cbValue]);
                })

                scope.$watch('cbData', function(){

                    scope.cbData.forEach(function(d){
                        $('<li />', {
                            text: d[scope.cbValue],
                            rel: d[scope.cbKey]
                        }).appendTo($selectOptions);


                        var $listItems = $selectOptions.children('li');

                        $listItems.on('click', function(e) {
                            e.stopPropagation();

                            $styledSelect.text($(this).text()).removeClass('active');
                            $selectOptions.css('display', "none");

                            scope.cbModel[scope.cbValue] = $(this).text();
                            scope.cbModel[scope.cbKey] = $(this).attr('rel');

                            scope.$apply();
                        });
                    })

                });

                $styledSelect.on('click', function(e) {
                    e.stopPropagation();

                    $('div.select-styled.active').not($styledSelect).each(function(){
                        $(this).removeClass('active').next().css('display', "none");
                    });

                    if ( $styledSelect.hasClass('active') )
                        $styledSelect.removeClass('active').next().css('display', "none");
                    else
                        $styledSelect.addClass('active').next().css('display', "block");

                });


                $(document).click(function() {

                    $('div.select-styled.active').each(function(){
                        $(this).removeClass('active').next().css('display', "none");
                    });

                });

            };

            return {
                restrict: 'A',
                replace: 'true',
                template: template,
                scope: {
                    cbModel: '=',
                    cbData: '=',
                    cbKey: '@',
                    cbValue: '@',
                    cbPlaceholder: '@'
                },
                link: link
            };

        })

        .directive('starRating', function() {
            return {
                restrict: 'EA',
                template:
                '<ul class="star-rating" ng-class="{readonly: readonly}">' +
                '  <li ng-repeat="star in stars" class="star" ng-class="{filled: star.filled}" ng-click="toggle($index)">' +

                '  </li>' +
                '</ul>',
                scope: {
                    ratingValue: '@',
                    max: '=?', // optional (default is 5)
                    onRatingSelect: '&?',
                    readonly: '=?'
                },
                link: function(scope, element, attributes) {
                    if (typeof scope.max == 'undefined') {
                        scope.max = 5;
                    }

                    initStars();

                    function initStars() {
                        scope.stars = [];
                        for (var i = 0; i < scope.max; i++) {
                            scope.stars.push({
                                filled: false
                            });
                        }
                    };

                    function updateStars() {
                        scope.stars = [];
                        for (var i = 0; i < scope.max; i++) {
                            scope.stars.push({
                                filled: i < scope.ratingValue
                            });
                        }
                    };

                    scope.toggle = function(index) {
                        if (typeof scope.readonly == 'undefined' || scope.readonly === false){
                            scope.ratingValue = index + 1;

                            if (typeof scope.onRatingSelect == 'function') {
                                scope.onRatingSelect({
                                    rating: index + 1
                                });
                            }

                        }
                    };

                    scope.$watch('ratingValue', function(oldValue, newValue) {
                        if (newValue) {
                            updateStars();
                        }
                    });
                }
            };
        });
})();




