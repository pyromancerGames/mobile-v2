(function () {
    angular.module('classbuzz.attendance')
        .factory('attendanceServices', function ($rootScope, $localStorage, $location, _, apiServices) {
            var services = {

                getAttendance: function (groupId, _date) {
                    var endpoint = '/attendance/group/'+groupId+'/date/'+_date;
                    return apiServices.GET(endpoint);
                },

                updateAttendance: function (_data) {
                    var endpoint = '/attendance';
                    return apiServices.PUT(endpoint,_data);
                }

            };
            return services;
        });

})();

