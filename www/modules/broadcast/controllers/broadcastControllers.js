angular.module('classbuzz.broadcast')

    .controller('BroadcastController', function ($http, $rootScope, $scope, $ionicModal, $ionicLoading, $ionicPopup,
                                                 authenticationServices, errorServices, broadcastServices,
                                                 userServices, groupServices) {

        $scope.groups = [];
        $scope.broadcastTo = {id:"all", name:"All"};
        $scope.broadcastOptions = [{id:"all", name:"All"}, {id:"group", name:"Group"}];
        $scope.selectedGroup = {id:"--", name:"--"};

        var loggedUser = authenticationServices.getProfile();

        $scope.changeSelectedGroup = function(newGroup){
            $scope.selectedGroup = newGroup;
        }

        $scope.changeBroadcastTo = function(newBroadcastTo){
            $scope.broadcastTo = newBroadcastTo;
        }

        // Editor options.
        $scope.editorOptions = {
            language: 'en',
            allowedContent: true,
            entities: false
        };

        // $scope.doBroadcastMessageWithConfirm = function(_subject, _content, _to, _group){
        //     var confirmPopup = $ionicPopup.confirm({
        //         title: 'Confirm Broadcast Message',
        //         template: 'Are you sure you want to broadcast this message?'
        //     });

        //     confirmPopup.then(function(res) {
        //         if(res) {
        //             doBroadcastMessage(_subject, _content, _to, _group);
        //         }
        //     });
        // }

        $scope.doBroadcastMessageWithConfirm = function(_subject, _content){
            var confirmPopup = $ionicPopup.confirm({
                title: 'Confirm Broadcast Message',
                template: 'Are you sure you want to broadcast this message?'
            });

            confirmPopup.then(function(res) {
                if(res) {
                    doBroadcastMessage(_subject, _content);
                }
            });
        }

        function doBroadcastMessage(_subject, _content){

            $ionicLoading.show({template:'<p>Initializing broadcast process, please wait.</p><ion-spinner></ion-spinner>'});

            var messageData = {
                customer: loggedUser.customer,
                branch: loggedUser.branch._id,
                teacher: loggedUser.id,
                group: $scope.selectedGroup._id,
                subject: _subject,
                body: _content
            };

            if (typeof $scope.selectedGroup == "undefined" || $scope.broadcastTo.id == 'all') messageData.group = null;
            else messageData.group = $scope.selectedGroup.id;

            broadcastServices.broadcastMessage(messageData)
                .then(function (result) {
                    $ionicLoading.hide();
                    $ionicLoading.show({template: '<p>Message process started, it may take a few minutes to complete...you may leave.</p>', duration:500});
                })

                .catch(function (err) {
                    $ionicLoading.hide();
                    $ionicLoading.show({template: 'We are embarrassed, there was an error on the mailing process. Please contact support', duration:2000});
                    errorServices.logError(err, "trying to populate groups/brodcastController");
                });
        }

        // function doBroadcastMessage(_subject, _content, _to, _group){

        //     $ionicLoading.show({template:'<p>Initializing broadcast process, please wait.</p><ion-spinner></ion-spinner>'});

        //     var messageData = {
        //         customer: loggedUser.customer,
        //         branch: loggedUser.branch,
        //         teacher: loggedUser.id,
        //         subject: _subject,
        //         body: _content
        //     };

        //     if (typeof _group == "undefined" || $scope.broadcastTo == 'all') messageData.group = null;
        //     else messageData.group = _group;

        //     broadcastServices.broadcastMessage(messageData)
        //         .then(function (result) {
        //             $ionicLoading.hide();
        //             $ionicLoading.show({template: '<p>Message process started, it may take a few minutes to complete...you may leave.</p>', duration:500});
        //         })

        //         .catch(function (err) {
        //             $ionicLoading.hide();
        //             $ionicLoading.show({template: 'We are embarrassed, there was an error on the mailing process. Please contact support', duration:2000});
        //             errorServices.logError(err, "trying to populate groups/brodcastController");
        //         });
        // }

        populateGroups();

        // LOCAL FUNCTIONS
        function populateGroups() {

                groupServices.getTeacherGroups(authenticationServices.userProfile.id, {condensed:true})

                .then(function (groups) {
                    $scope.groups = groups.data;
                })

                .catch(function (err) {
                    errorServices.logError(err, "trying to populate groups/brodcastController");
                });
        }

    });

