(function () {
    angular.module('classbuzz.broadcast')
        .factory('broadcastServices', function ($rootScope, $localStorage, $location, _, apiServices) {
            var services = {

                broadcastMessage: function (_data) {
                    var endpoint = '/mail/adhoc';
                    return apiServices.POST(endpoint,_data);
                },


            };
            return services;
        });

})();
