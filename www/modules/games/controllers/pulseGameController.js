angular.module('classbuzz.games')

    .controller('PulseGameController', function ($http, $rootScope, $ionicListDelegate, $ionicLoading, $state, _, $timeout,
                                                    $socketio, $moment, $scope, notificationServices, authenticationServices,
                                                    errorServices, $translate, $ionicPopup) {

        $scope.joinSession = joinSession;
        $scope.createSession = createSession;
        $scope.allReady = allReady;
        $scope.isOwner = isOwner;
        $scope.isTeacher = isTeacher;
        $scope.imReady = imReady;
        $scope.imNotReady = imNotReady;
        $scope.pauseSession = pauseSession;
        $scope.resumeSession = resumeSession;
        $scope.startSession = startSession;
        $scope.correctAnswer = correctAnswer;
        $scope.incorrectAnswer = incorrectAnswer;
        $scope.leaveSession = leaveSession;
        $scope.pulse = pulse;
        $scope.countdown = -1;

        $scope.gameOn = false;
        $scope.amIReady = false;
        $scope.session = null;
    
        var user = authenticationServices.userProfile;

        function pulse () {
            $socketio.emit('session.event', $scope.session._id, user.id, "pulse.pulse");
        }

        function correctAnswer () {
            $socketio.emit( 'session.event', $scope.session._id, user.id, 'pulse.pulse.evaluate', {correct:true} );
        }

        function incorrectAnswer () {
            $socketio.emit( 'session.event', $scope.session._id, user.id, 'pulse.pulse.evaluate', {correct:false} );
        }

        function joinSession ( sessionId ) {
            if (_.isNil(sessionId)) return;
            $socketio.emit('session.join', sessionId, user.id);
        }

        function createSession () {
            $socketio.emit('session.create', user.id, "play.pulse");
        }
        
        function imReady () {
            $socketio.emit('session.event', $scope.session._id, user.id, 'user.ready');
        }

    
        function isTeacher () {
            return authenticationServices.hasRole('teacher');
        }

        function imNotReady () {
            $socketio.emit('session.event', $scope.session._id, user.id, 'user.unready');
        }

        function allReady () {
            var allready = true;
            $scope.session.connected.forEach(function(c){
                allready = allready && (c.status == 'ready');
            })

            return allready;
        }

        function isOwner () {
            return $scope.session.owner.toString() == user.id.toString();
        }

        function startSession () {
            $scope.countdown = "READY";
            $socketio.emit('session.event', $scope.session._id, user.id, 'game.start');
        }
        
        function pauseSession () {
            $socketio.emit('session.event', $scope.session._id, user.id, 'game.pause');
        }

        function resumeSession () {
            $socketio.emit('session.event', $scope.session._id, user.id, 'game.resume');
        }

        function leaveSession () {
            $translate(['titles.confirm', 'messages.confirm_end_session']).then(function(trans){
                var confirmPopup = $ionicPopup.confirm({
                    title: trans['titles.confirm'],
                    template: trans['messages.confirm_end_session'] + '?'
                });

                confirmPopup.then(function(res) {
                    if(res) {
                        $socketio.emit('session.leave', $scope.session._id, user.id);
                    }
                });
            });
        }

        function setLocalStatus(userId, status) {
            
            $scope.session.connected.forEach(function(c){
                if (c.user._id.toString() == userId.toString()) {
                    c.status = status;
                }
            })

            // //Its my status
            if (userId.toString() == user.id.toString()) {
                $scope.amIReady = status == 'ready';
            }
        }

        $socketio.on('session.created', function (_session) {
            $scope.session = _session;
        });
        
        $socketio.on('session.countdown.tick', function (_session, count) {
            $scope.session = _session;
            $scope.countdown = count;
            $scope.gameOn = true;
        });
        
        $socketio.on('session.game.start', function (_session) {    
            $scope.session = _session;
            $scope.countdown = "GO";

            $timeout(function(){
                $scope.countdown = -1;
            },1000);
            
        });

        $socketio.on('session.created', function (_session) {
            $scope.session = _session;
        });
        
        $socketio.on('session.destroyed', function (_session) {
            $scope.session = null;
            $scope.gameOn = false;
            $scope.amIReady =  false;
        });

        $socketio.on('session.user.left', function (_session, _user) {
            $scope.session = _session;
            if (_user._id.toString() == user.id.toString()) {
                $scope.session = null;
                $scope.gameOn = false;
                $scope.amIReady =  false;
            }   
        });
        
        $socketio.on('session.status.change', function (_session, _status) {
            $scope.session = _session;
            $scope.session.status = _status;
        });
        
        $socketio.on('session.user.joined', function (_session, _user) {
            $scope.session = _session;
            if (_user._id.toString() == user.id.toString() && user.id.toString() == _session.owner.toString()) {    
                //Its me == owner == always ready,
                imReady();
            }
            
        });
        
        $socketio.on('session.pulse.pulse', function (_session, _user) {
            $scope.session = _session;
            $scope.pulsingUser = _user;
            $scope.pulsingUser.action = "PULSED BY:"
        });


        $socketio.on('session.pulse.question.answered', function (_session, _user) {
            $scope.session = _session;
            $scope.pulsingUser = _user;
            $scope.pulsingUser.action = "ANSWERED!"
        });
        
        $socketio.on('session.pulse.rebound', function (_session, _user) {
            $scope.session = _session;
            $scope.pulsingUser = _user;
            $scope.pulsingUser.action = "REBOUND TO:"
        });

        $socketio.on('session.user.status', function (_user, _status) {
            
            //Update user status
            setLocalStatus(_user._id, _status);
        });


    });


