angular.module('classbuzz.store')

    .controller('StoreController', function ($http, $rootScope, $scope, $socketio, $moment,
                                                    $ionicModal, $ionicPopup, $ionicScrollDelegate, authenticationServices, $ionicLoading,
                                                    errorServices, userServices, storeServices, $location, $translate, $timeout, _) {
        // Scroll handlers
        var traitsTypesHandler = $ionicScrollDelegate.$getByHandle('traitsTypesHandler');
        var traitsPartsHandler = $ionicScrollDelegate.$getByHandle('traitsPartsHandler');

        $scope.currentSidebarItem = 2;
        $scope.mSelected = 'body';
        $scope.pSelected = 'hair';
        $scope.traitsLoaded = false;
        $scope.allTraits = [];
        $scope.itemsPerPage = 10;
        $scope.page = 1;
        $scope.user = {};
        $scope.character = null;
        $scope.firstTimeLoaded = true;
        $scope.equippedTraits = [];
        $scope.equipEnabled = false;
        $scope.dirtyTrait = false;
        $scope.svgLoaded = false;
        $scope.traitNavEnabled = true;
        $scope.storeIsLoading = true;
        $scope.displayClearButton = false;
        var backImgSuffix = '-back';

        PopulateTopNavTraits();

        $scope.traitsNavList = [
            {name: 'bodyColour', mType: 'body', markupId: 'item1', imgPath: 'img/assets/store/store_icon_body.svg'},
            {name: 'hair', mType: 'body', markupId: 'item2', imgPath: 'img/assets/store/store_icon_hair.svg'},
            {name: 'eyebrow', mType: 'body', markupId: 'item3', imgPath: 'img/assets/store/store_icon_eyebrows.svg'},
            {name: 'eyes', mType: 'body', markupId: 'item4', imgPath: 'img/assets/store/store_icon_eyes.svg'},
            {name: 'mouth', mType: 'body', markupId: 'item5', imgPath: 'img/assets/store/store_icon_mouth.svg'},
            {name: 'facialHair', mType: 'body', markupId: 'item6', imgPath: 'img/assets/store/store_icon_mustache.svg'},
            {name: 'eyes', mType: 'wearable', markupId: 'item7', imgPath: 'img/assets/store/store_icon_sunglasses.svg'},
            {name: 'chest', mType: 'wearable', markupId: 'item8', imgPath: 'img/assets/store/store_icon_clothes1.svg'},
            {name: 'legs', mType: 'wearable', markupId: 'item9', imgPath: 'img/assets/store/store_icon_clothes2.svg'},
            {name: 'feet', mType: 'wearable', markupId: 'item10', imgPath: 'img/assets/store/store_icon_footwear.svg'},
            {name: 'head', mType: 'wearable', markupId: 'item11', imgPath: 'img/assets/store/store_icon_hat.svg'},
            {name: 'rightHand', mType: 'wearable', markupId: 'item12', imgPath: 'img/assets/store/store_icon_hammer.svg'},
            {name: 'companionRight', mType: 'wearable', markupId: 'item13', imgPath: 'img/assets/store/store_icon_pet.svg'}
        ];

        // Get user
        userServices.getUser(authenticationServices.getUserId())
            .then(function(usrRes) {
                // Set user
                $scope.user = usrRes.data;
                // Get character
                userServices.getUserCharacter(authenticationServices.getUserId())
                    .then(function(charRes) {

                        $scope.character = charRes.data;
                        $scope.firstTimeLoaded = $scope.character.traits.body.upperBody == null;

                        // Traits
                        if($scope.firstTimeLoaded) {

                            var charGender = authenticationServices.getCharGender() || 'M';

                            storeServices.getDefaultTrait(charGender)
                                .then(function(res) {
                                    // Equip
                                    $scope.equippedTraits = res.data;
                                    setAvailableForUserDefaults();
                                    PopulateTopNavTraits();
                                    // Persist default traits on character traits
                                    $scope.updateCharEquipedTraits();
                                    displayCurrentTraits($scope.equippedTraits);
                                })
                        } else {
                            // Equipped
                            $scope.equippedTraits = $scope.character.traits;
                            displayCurrentTraits($scope.equippedTraits);
                            // TODO: implement
                            $scope.equippedTraits.body.eyebrow = $scope.equippedTraits.body.eyebrows;
                            setAvailableForUserDefaults();
                            PopulateTopNavTraits();
                        }
                        $scope.storeIsLoading = false;

                    })
            })
            .catch(function(err){
                errorServices.logError(err, "Loading user avatar");
            });


        /*
        * Sets default traits available for user to equip
        */
        function setAvailableForUserDefaults() {

            for (var key in $scope.equippedTraits.body) {
                if($scope.equippedTraits.body[key]) {
                    $scope.equippedTraits.body[key].availableForUser = true;
                }
            }

            for (var key in $scope.equippedTraits.wearables) {
                if($scope.equippedTraits.wearables[key]) {
                    $scope.equippedTraits.wearables[key].availableForUser = true;
                }
            }
        }

        // Traits side menu Start
        $scope.scrollDown = function(){
            $timeout(function(){
                traitsTypesHandler.scrollBy(0, -60, [true])
            });
        };

        $scope.scrollUp = function(){
            $timeout(function(){
                traitsTypesHandler.scrollBy(0, 60, [true])
            });
        };


        $scope.scrollLeft= function(){
            $timeout(function(){
                traitsPartsHandler.scrollBy(-80, 0, [true])
            });
        };

        $scope.scrollRight= function(){
            $timeout(function(){
                traitsPartsHandler.scrollBy(80, 0, [true])
            });
        };

        $scope.scrlTo = function(tt){
            if($scope.traitNavEnabled) {
                $scope.traitNavEnabled = false;
                $timeout(enableTraitNav, 800);
                $scope.currentSidebarItem = $scope.traitsNavList.indexOf(tt) + 1;
                SetSelectedNavProps(tt);
                PopulateTopNavTraits();
            }
        };


        function enableTraitNav() {
            $scope.traitNavEnabled = true;
        }

        function SetSelectedNavProps(tt) {
            $scope.mSelected = tt.mType;
            $scope.pSelected = tt.name;
        }

        // Top nav update
        function PopulateTopNavTraits() {

            if($scope.traitsLoaded){
                traitsPartsHandler.scrollTop();
            }

            $scope.traitsLoaded = false;
            // Clear traits list
            $scope.allTraits = [];

            // Reset page
            $scope.page = 1;
            // Reset all traits loaded
            $scope.noMoreItemsAvailable = false;

            var  filters = {
                traitType: $scope.mSelected,
                placement: $scope.pSelected
            };

            storeServices.getTraits('', filters, $scope.itemsPerPage, $scope.page)
                .then( function(res) {
                    // First populate
                    $scope.allTraits.push.apply($scope.allTraits, res.data[0]);
                    $scope.totalItems = res.data[2];
                    filterAvailableTraits(); // only for new items (res.data[0])
                    checkDirtyTraits();
                    checkClearDisplayButton();
                    $scope.traitsLoaded = true;
                })
                .catch (function (err) {
                    errorServices.logError(err, "Populating avatar top nav menu");
                })

        }

        // Clear selected part
        $scope.clearCurrentPart = function () {

            var partModel = $scope.mSelected == 'wearable' ? 'wearables' : $scope.mSelected;

            // Create a copy of the current (old) traits
            var oldTraits = angular.copy($scope.equippedTraits);
            // Disable previous traits
            hideOldTraits(oldTraits);

            if($scope.pSelected == 'eyebrow') {
                $scope.equippedTraits[partModel]['eyebrows'] = null;
            } else {
                $scope.equippedTraits[partModel][$scope.pSelected] = null;
            }
            // Display current traits
            displayCurrentTraits($scope.equippedTraits);

            $scope.storeIsLoading = false;
            $scope.equipEnabled = true;
            checkDirtyTraits();

        };


        function setAvatarURL() {

            var svgAvatar = document.getElementById("store-avatar-svg");
            canvg(document.getElementById('mycanvas'), svgAvatar.innerHTML);
            var canvas = document.getElementById("mycanvas");
            // Set trait img
            userServices.updateUserAvatar(canvas.toDataURL())
                .then(function(res) {
                    console.log("User successfully updated");
                    $scope.$emit('avatarupdated');
                    $scope.storeIsLoading = false;
                })
                .catch(function(err) {
                    $scope.storeIsLoading = false;
                    errorServices.logError(err, "updating avatar URL");
                })
        }

        $scope.updateCharEquipedTraits = function () {
            $scope.storeIsLoading = true;

            // Set new values
            var newChar = JSON.parse(JSON.stringify($scope.character));

            newChar.traits.body = parseBodyIds();
            newChar.traits.wearables = parseWearablesIds();
            newChar.traits.companion = null;
            newChar.inventory = $scope.character.inventory;

            userServices.updateChar(newChar, authenticationServices.getUserId())
                .then(function(res) {
                    // Store img on avatar URL and update user avatar
                    $scope.equipEnabled = false;
                    // Store img on avatar URL and update user avatar
                    setAvatarURL();
                    $scope.$emit('avatarupdated');
                    $scope.storeIsLoading = false;
                })
                .catch(function(err) {
                    errorServices.logError(err, "Error while trying to update character traits");
                });
        }

        function filterAvailableTraits() {

            if($scope.character !== null) {

                if(!$scope.character.inventory) {
                    $scope.character.inventory = [];
                }

                $scope.allTraits.forEach(function(element) {

                    // Init default value as not available
                    element.availableForUser = false;
                    var i = $scope.character.inventory.indexOf(element._id);
                    // Exists inside charInventory or it's free
                    if(i != -1 || element.price == 0 && element.requirements.level <= $scope.character.levelInfo.level) {
                        element.availableForUser = true;
                        return;
                    }

                    // Available to buy
                    //element.userCanBuy = charCanBuyTrait(element);
                    //element.userCanBuy = storeServices.charCanBuyTrait($scope.avatarLoading, element, $scope.user.student, $scope.character);

                }, this);
            }
        }

        $scope.checkSelectedBodyColour = function(val) {

            $scope.storeIsLoading = true;

            var upperBody;
            var lowerBody;
            var	head;

            storeServices.getSingleTrait('Body-Head-' + val)
                .then(function(resHead) {
                    head = resHead.data;
                    head.availableForUser = true;
                    storeServices.getSingleTrait('Body-Legs-' + val)
                        .then(function(resLowerbody) {
                            lowerBody = resLowerbody.data;
                            lowerBody.availableForUser = true;
                            storeServices.getSingleTrait('Body-Chest-' + val)
                                .then(function(resUp) {
                                    upperBody = resUp.data;
                                    upperBody.availableForUser = true;

                                    // Create a copy of the current (old) traits
                                    var oldTraits = angular.copy($scope.equippedTraits);
                                    // Disable previous traits
                                    hideOldTraits(oldTraits);

                                    $scope.equippedTraits['body'][upperBody.placement] = upperBody;
                                    $scope.equippedTraits['body'][lowerBody.placement] = lowerBody;
                                    $scope.equippedTraits['body'][head.placement] = head;

                                    displayCurrentTraits($scope.equippedTraits);

                                    // Set trait color;
                                    $scope.equipEnabled = true;

                                    checkDirtyTraits();

                                    $scope.storeIsLoading = false;
                                })
                        })
                })
                .catch(function(err) {
                    errorServices.logError(err, "Error while trying to get avatar body colour");
                });
        };

        // Set preview part
        $scope.setPreviewPart = function(trait){

            // Loading
            $scope.storeIsLoading = true;
            // Equip
            $scope.equipEnabled = true;

            // Get selected part
            var partModel = $scope.mSelected == 'wearable' ? 'wearables' : $scope.mSelected;

            // Create a copy of the current (old) traits
            var oldTraits = angular.copy($scope.equippedTraits);

            // Disable previous traits
            hideOldTraits(oldTraits);

            // Set new trait
            if(trait.placement == 'eyebrow') {
                $scope.equippedTraits[partModel]['eyebrows'] = trait;
            }
            $scope.equippedTraits[partModel][trait.placement] = trait;

            // Enable current traits
            displayCurrentTraits($scope.equippedTraits);
            checkDirtyTraits();
            // !Loading
            $scope.storeIsLoading = false;

        };

        // Hide previous equiped traits on update
        function hideOldTraits(oldTraits) {
            // Hide body elements
            setTraitHTMLElementAttributes(oldTraits, 'body', 'display', 'none');
            // Hide wearable elements
            setTraitHTMLElementAttributes(oldTraits, 'wearables', 'display', 'none');
        }

        // Display new equiped traits
        function displayCurrentTraits(newTraits) {
            if($scope.svgLoaded){
                // Display body elements
                setTraitHTMLElementAttributes(newTraits, 'body', 'display', 'inline');
                // Display wearable elements
                setTraitHTMLElementAttributes(newTraits,'wearables', 'display', 'inline');
            } else {
                $timeout(function() {
                    displayCurrentTraits($scope.equippedTraits)
                }, 2000);
            }
        }

        // Set traits attrs
        function setTraitHTMLElementAttributes(traits, forType, att, val) {
            for (var k in traits[forType]){
                if (traits[forType][k] !== null && typeof traits[forType][k] !== 'function') {
                    // Fore image
                    angular.element( document.querySelector('#' + traits[forType][k].name) ).attr(att, val);
                    // Back image
                    if(traits[forType][k].backImageURL != null) {
                        angular.element( document.querySelector('#' + traits[forType][k].name + backImgSuffix) ).attr(att, val);
                    }
                }
            }
        }

        function checkClearDisplayButton() {

            var partModel = $scope.mSelected == 'wearable' ? 'wearables' : $scope.mSelected;

            if(partModel == 'body') {
                // Body
                if($scope.pSelected == 'hair' || $scope.pSelected == 'facialHair') {
                    $scope.displayClearButton = true;
                } else {
                    $scope.displayClearButton = false;
                }

            } else {
                // Wearables
                if($scope.pSelected == 'chest' || $scope.pSelected == 'legs') {
                    $scope.displayClearButton = false;
                } else {
                    $scope.displayClearButton = true;
                }
            }
        }


        function parseBodyIds() {

            var traitsBody = JSON.parse(JSON.stringify($scope.equippedTraits.body));

            for (var key in traitsBody) {
                if(traitsBody[key]) {
                    traitsBody[key] = traitsBody[key]._id;
                }
            }

            return traitsBody;
        }

        function parseWearablesIds() {

            var traitsWearables = JSON.parse(JSON.stringify($scope.equippedTraits.wearables));

            for (var key in traitsWearables) {
                if(traitsWearables[key]) {
                    traitsWearables[key] = traitsWearables[key]._id;
                }
            }

            return traitsWearables;
        }

        $scope.checkEquipedTraits = function () {
            return $scope.equipEnabled && !$scope.dirtyTrait;
        }

        // Checks that the user doesn't have a preview item equipped
        function checkDirtyTraits() {

            for (var key in $scope.equippedTraits.body) {
                if($scope.equippedTraits.body[key]) {
                    if(!$scope.equippedTraits.body[key].availableForUser) {
                        $scope.dirtyTrait = true;
                        return;
                    }
                }
            }

            for (var key in $scope.equippedTraits.wearables) {
                if($scope.equippedTraits.wearables[key]) {
                    if(!$scope.equippedTraits.wearables[key].availableForUser) {
                        $scope.dirtyTrait = true;
                        return;
                    }
                }
            }

            $scope.dirtyTrait = false;
            return;
        }

        $scope.svgLoadedComplete = function() {
            $scope.svgLoaded = true;
        };

        // Reset character values
        $scope.cancelUpdate = function () {
            $scope.storeIsLoading = true;
            userServices.getUserCharacter(authenticationServices.getUserId())
                .then(function(res) {
                    // Equipped
                    $scope.character = res.data;
                    /// Create a copy of the current (old) traits
                    var oldTraits = angular.copy($scope.equippedTraits);
                    // Disable previous traits
                    hideOldTraits(oldTraits);
                    $scope.equippedTraits = $scope.character.traits;
                    displayCurrentTraits($scope.equippedTraits);
                    setAvailableForUserDefaults();
                    $scope.storeIsLoading = false;
                    $scope.equipEnabled = false;
                })
                .catch(function(err){
                    errorServices.logError(err, "Error while trying to get user avatar");
                });
        }
        // Traits side menu End


        // Buy trait
        $scope.buyTrait = function(trait) {

            $translate(["titles.confirm_buy", "messages.confirm_buy", "buttons.yes"])
                .then(function(translations){

                    var confirmPopup = $ionicPopup.confirm({
                        title: translations["titles.confirm_buy"],
                        template: translations["messages.confirm_buy"]
                    });

                    confirmPopup.then(function(res) {
                        if(res) {
                            
                            $translate("messages.fetching_profile").then(function(fetchingText){
                                fetchingText = '<p>'+fetchingText+'</p><ion-spinner></ion-spinner>';
                                $ionicLoading.show({template:fetchingText});
                            });

                            userServices.getUserCharacter(authenticationServices.getUserId())
                                .then(function(res) {
                                    $ionicLoading.hide();

                                    $scope.character = res.data;

                                    var canBuyTrait = charCanBuyTrait(trait);

                                    if(canBuyTrait) {

                                        if(!$scope.character.inventory) {
                                            $scope.character.inventory = [];
                                        }
                                        $scope.equipEnabled = true;
                                        // Update user bought traits
                                        updateBoughtTraits(trait);
                                    } else {
                                        $scope.tooltips.cannotBuyTrait();
                                        console.log('User cannot buy this item');
                                    }
                                })
                                .catch(function(err) {

                                    $translate("messages.embarrassing_error").then(function(errorText){
                                        $ionicLoading.hide();
                                        errorText = '<p>' + errorText + '</p>';
                                        $ionicLoading.show({template: errorText, duration:2000});
                                        errorServices.logError(err, "fetching full user profile on store");
                                    });

                                    errorServices.logError(err, "Error while trying to buy trait");
                                })
                        } else {
                            console.log("Buy canceled");
                        }
                    });
                });
        };

        $scope.tooltips = {
            cannotBuyTrait: function() {

                var tooltip = {
                    title: "Trait unavailable",
                    text: "You can't buy this trait yet!",
                    btnText: "Got it"
                };

                $rootScope.$broadcast('cb.showtooltip', tooltip);
            }
        };

        function updateBoughtTraits(trait) {

            // Set new values
            $scope.storeIsLoading = true;

            userServices.buyTrait(trait._id, authenticationServices.getUserId())
                .then(function(res) {
                    $scope.user.student.currency = res.data.currency;
                    $scope.character.inventory = res.data.inventory;

                    // Enable equip
                    var partModel = $scope.mSelected == 'wearable' ? 'wearables' : $scope.mSelected;
                    $scope.equippedTraits[partModel][trait.placement].availableForUser = true;

                    $scope.equipEnabled = true;
                    PopulateTopNavTraits();
                    $scope.$emit('itembought');
                    $scope.storeIsLoading = false;
                })
                .catch(function(err) {
                    errorServices.logError(err, "Error while trying to update bought traits");
                    $scope.storeIsLoading = false;
                })
        }

        function charCanBuyTrait(trait) {
            var canBuyTrait = true;

            if(!$scope.storeIsLoading) {

                if($scope.user.student){

                    // Check user doublouns
                    if(trait.price > $scope.user.student.character.money) {
                        canBuyTrait = false;
                    }

                    // Check level req
                    if(canBuyTrait && trait.requirements.level > $scope.character.levelInfo.level) {
                        canBuyTrait = false;
                    }

                    // Check trait req
                    if(canBuyTrait && trait.requirements.trait) {
                        canBuyTrait = checkForRequieredTrait(trait);
                    }
                }

                return canBuyTrait;

            } else {
                return false;
            }

        }

        // Check requirements to buy a trait
        function checkForRequieredTrait(trait) {

            var hasReq = false;

            $scope.character.inventory.forEach(function(element) {

                var rq = trait._id == element;

                if(rq) {
                    // Exists inside charInventory
                    hasReq = true;
                    return;
                }

            }, this);

            return hasReq;
        }

        // Check active part
        $scope.checkActive = function (trait) {
            if(!$scope.storeIsLoading) {
                var partModel = $scope.mSelected == 'wearable' ? 'wearables' : $scope.mSelected;
                if($scope.equippedTraits[partModel] && $scope.equippedTraits[partModel][trait.placement]) {
                    if($scope.equippedTraits[partModel][trait.placement]['foreImageURL'] == trait.foreImageURL) {
                        return true;
                    }
                }
            }
            return false;
        };

        $scope.checkActiveBodyColour = function (traitName) {
            if(!$scope.storeIsLoading) {
                return $scope.equippedTraits.body.upperBody.name == traitName;
            }
            return false;
        };

        // Display/hide price if the user already have the item
        $scope.userHasTrait = function(trait) {
            return false;
        }

        // Load more items (infinite scroll)
        $scope.noMoreItemsAvailable = false;
        $scope.number = 10;
        $scope.items = [];

        $scope.loadMoreTraits = function() {

            if($scope.traitsLoaded) {
                $scope.traitsLoaded = false;

                var  filters = {
                    traitType: $scope.mSelected,
                    placement: $scope.pSelected
                };
                $scope.page++;

                storeServices.getTraits('', filters, $scope.itemsPerPage, $scope.page)
                    .then( function(res) {
                        // First populate
                        $scope.allTraits.push.apply($scope.allTraits, res.data[0]);
                        $scope.traitsLoaded = true;
                        filterAvailableTraits();
                        if ( $scope.allTraits.length >= $scope.totalItems ) {
                            $scope.noMoreItemsAvailable = true;
                        }
                    })
                    .catch (function (err) {
                        errorServices.logError(err, "Error while trying to load more traits");
                    });
            }
            $scope.$broadcast('scroll.infiniteScrollComplete');
        };
    });
