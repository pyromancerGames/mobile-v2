angular.module('classbuzz.utils.camera')
    .factory('cameraServices', function ( $cordovaCamera, $cordovaImagePicker ) {

        var services = {};


        /**
         * Opens the camera and lets user take a picture.
         * @returns {Promise}
         */
        services.takePicture = function () {

            return new Promise(function(resolve, reject){

                var options = {
                    quality: 50,
                    destinationType: Camera.DestinationType.FILE_URI,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    allowEdit: true,
                    encodingType: Camera.EncodingType.JPEG,
                    targetWidth: 100,
                    targetHeight: 100,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: false,
                    correctOrientation:true
                };

                $cordovaCamera.getPicture(options).then(function(imageURI) {
                    resolve(imageURI);
                }, function(err) {
                    reject (err);
                });

            });

        };

        /**
         * Lets select picture from camera roll.
         * @returns {Promise} The local URI of selected picture.
         */
        services.selectPicture = function () {

            return new Promise(function(resolve, reject){

                var options = {
                    maximumImagesCount: 1,
                    width: 800,
                    height: 800,
                    quality: 80
                };

                $cordovaImagePicker.getPictures(options)
                    .then(function (results) {
                        resolve(results[0])
                    }, function(err) {
                        reject (err);
                    });

            });

        };


        return services;
    });





