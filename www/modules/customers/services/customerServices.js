(function () {
    angular.module('classbuzz.customers')
        .factory('customerServices', function ($rootScope, $localStorage, $location, _, apiServices,
                                            authenticationServices) {
            var services = {

                /**
                 * Get events for a customer
                 * @param [customerId] - defaults to logged user customer
                 * @returns {*}
                 */
                getNextSorting: function (customerId) {

                    if (typeof customerId == "undefined" || customerId == null) customerId = authenticationServices.userProfile.customer;

                    var endpoint = '/customers/'+customerId+'/sorting/next';
                    return apiServices.GET(endpoint);
                }
            };

            return services;
        });

})();




