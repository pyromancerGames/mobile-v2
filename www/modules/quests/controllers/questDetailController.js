angular.module('classbuzz.quests')

    .controller('QuestDetailController', function ($http, $rootScope, $scope, $socketio, $moment,
                                                    $ionicModal, $ionicListDelegate, $stateParams, authenticationServices,
                                                    errorServices, userServices, cbUtils) {


        $scope.userQuests = [];
        $scope.questInfo = {};
        $scope.doRefresh = doRefresh;
        $scope.fromNow = cbUtils.fromNow;

        getQuestInfo();

        function doRefresh(){
            getQuestInfo();
        }

        function getQuestInfo () {
            //Get all active quests.
            userServices.getActiveQuests()
                .then(function(result){
                    $scope.$broadcast('scroll.refreshComplete'); // Stop the ion-refresher from spinning

                    //Store that info for later.
                    $scope.userQuests = result.data;

                    console.log(result.data);

                    //Find and extract the corresponding detail quest.
                    result.data.forEach(function(q){
                        if (q.quest._id == $stateParams.questId ) $scope.questInfo = q;
                    });

                    console.log($scope.questInfo);
                })
        }


    });

