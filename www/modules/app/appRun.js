(function () {
    angular.module('griffinmobile')

        .run(function ($rootScope, $http, $ionicPlatform, $cordovaSplashscreen, ngFB) {

            $ionicPlatform.ready(function () {

                ngFB.init({appId: '1774330206188338'});

                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                if (window.cordova && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                    cordova.plugins.Keyboard.disableScroll(true);
                }

                if (window.StatusBar) {
                    // org.apache.cordova.statusbar required
                    StatusBar.styleDefault();
                }

                // Hide splash screen
                setTimeout(function() {
                    $cordovaSplashscreen.hide();
                }, 5000)

            })


            $rootScope.gfConfig =
            {
                // "baseURL": "http://54.94.155.99:5000",
                // "baseURL": "http://localhost:5000",
                // "baseURL": "http://192.168.1.100:5000",
                // "baseURL": "http://api.classbuzz.edu.uy",
                "baseURL": "http://ec2-54-232-242-71.sa-east-1.compute.amazonaws.com:5000",
                "apiVersion": "/api/v2",
                "appKey": "ce0998626306f550dd9d88e4ef2c98fff5f8aabc",
                "avatarPlaceholder": "img/avatar-placeholder.png",
                "backgroundImage": "img/background.png",
                "s3bucket" : "pyromancer.co.gg.images",
                "statsMonthLabels": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
            };

        })
})();
