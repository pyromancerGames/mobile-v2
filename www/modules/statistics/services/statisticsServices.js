(function() {
    /**
     * @ngdoc function
     * @name classbuzz.statistics
     * @description
     * Services of the class buzz statistics
     */
    angular.module('classbuzz.statistics')
        .factory('statisticsServices', ['$rootScope',  'apiServices',  'authenticationServices', function ($rootScope, apiServices, authenticationServices) {

            var StatisticsServices = {

                /**
                 * Get Statistics for Teacher (yearly)
                 * @param teacherId
                 * @returns {*}
                 */
                getTeacherActivities: function (teacherId) {

                    var _endpoint = '/statistics/teacher/'+teacherId+'/activities';
                    return apiServices.GET(_endpoint);

                },

                /**
                 * Get Statistics for Student (activity)
                 * @param studentId
                 * @returns {*}
                 */
                getStudentActivity:function (studentId) {
                    if (typeof studentId == 'undefined') studentId = authenticationServices.userProfile.id;
                    var _endpoint = '/statistics/student/'+studentId+'/activities';
                    return apiServices.POST(_endpoint);

                },


                /**
                 * Get Statistics for Student (attendance)
                 * @param studentId
                 * @returns {*}
                 */
                getStudentAttendance:function (studentId) {
                    if (typeof studentId == 'undefined') studentId = authenticationServices.userProfile.id;
                    var _endpoint = '/statistics/student/'+studentId+'/attendance';
                    return apiServices.POST(_endpoint);

                }

            }
            return StatisticsServices;
        }]);

})();
