(function () {
    angular.module('classbuzz.achievements')
        .factory('achievementServices', function ($rootScope, $localStorage, $location, _, apiServices,
                                                  authenticationServices) {
            var services = {

                /**
                 * Gets all achievements
                 * @returns {*}
                 */
                getAchievements: function () {

                    var endpoint = '/achievements';
                    return apiServices.GET(endpoint);
                },


                /**
                 * Get all achievements for a customer
                 * @param [customerId] - Defaults to logged user's customer
                 * @returns {*}
                 */
                getCustomerAchievements: function (customerId) {
                    if (typeof customerId == "undefined" || customerId == null) customerId = authenticationServices.userProfile.customer;
                    var endpoint = '/achievements/customer/' + customerId;
                    return apiServices.GET(endpoint);
                },

            };
            return services;
        });

})();


