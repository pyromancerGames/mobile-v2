angular.module('classbuzz.groups')

    .controller('GroupsController', function (
        $http, $rootScope, $scope, $socketio, $ionicPopover, $state,
        $moment, $ionicModal, $ionicListDelegate, $ionicLoading,
        authenticationServices, errorServices, userServices,
        groupServices, $ionicPopup, $translate) {

        if(!authenticationServices.userIsLoggedIn()) {
            $state.go("app.home");
            return;
        }

        // SCOPE VARS
        $scope.groups = [];
        $scope.newGroupData = {};
        $scope.editGroupData = {};
        $scope.groupLevels = [];
        $scope.loadingGroups = true;
        $scope.showSearch = false;
        $scope.searchField = {
            value: ""
        };

        $scope.broadcastTo = "all";
        $scope.broadcastOptions = [{id:"all", name:"All"}, {id:"group", name:"Group"}];

        populateGroups();
        loadGroupLevels();

        $scope.toggleSearch = function() {
            $scope.showSearch = !$scope.showSearch;
            $scope.searchField.value = "";
        };

        // SCOPE FUNCTIONS
        $scope.createGroup = function () {
            $scope.newGroupData.level = {};
            $scope.modal = $scope.newGroupModal;
            $scope.modal.show();
        }

        $scope.editGroup = function (group) {
            $scope.editGroupData = group;
            $scope.modal = $scope.editGroupModal;
            $scope.modal.show();
        }

        $scope.doRefresh = function () {
            $scope.loadingGroups = true;
            populateGroups();
        }

        //Create the popovers
        $ionicPopover.fromTemplateUrl('modules/groups/views/groupsPopover.html', {scope: $scope})
            .then(function(popover) {
                $scope.popover = popover;
        });

        $scope.openPopover = function($event) {
            $scope.popover.show($event);
        };
        $scope.closePopover = function() {
            $scope.popover.hide();
        };
        //Cleanup the popover when we're done with it!
        $scope.$on('$destroy', function() {
            $scope.popover.remove();
        });
        // Execute action on hide popover
        $scope.$on('popover.hidden', function() {
            // Execute action
        });
        // Execute action on remove popover
        $scope.$on('popover.removed', function() {
            // Execute action
        });

        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('modules/groups/views/newGroup.html', {scope: $scope})
            .then(function (modal) {
                $scope.newGroupModal = modal;
            });

        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('modules/groups/views/editGroup.html', {scope: $scope})
            .then(function (modal) {
                $scope.editGroupModal = modal;
            });

        // Create the login modal that we will use later
        /*$ionicModal.fromTemplateUrl('modules/core/views/modalConfirmDeletion.html', {scope: $scope})
            .then(function (modal) {
                $scope.modalConfirmDeletion = modal;
            });*/

        // Triggered in the login modal to close it
        $scope.closeModal = function () {
            $scope.modal.hide();
            $ionicListDelegate.closeOptionButtons(); //Close swipe cards
        };

        // Navigate to group
        $scope.navigateToGroup = function (group) {
            $state.go("app.groupStudents", {'groupId' : group._id});
            return;
        };


        $scope.doCreateGroup = function () {
            $scope.newGroupData.customer = authenticationServices.userProfile.customer;
            $scope.newGroupData.teacher = authenticationServices.userProfile.id;

            if (typeof $scope.newGroupData.name == 'undefined' || $scope.newGroupData.name == ''){
                $ionicLoading.show({template: 'You cant create a group with an empty name', duration:2000});
                return;
            }

            if (typeof $scope.newGroupData.level.code == 'undefined'){
                $ionicLoading.show({template: 'You cant create a group without a level', duration:2000});
                return;
            }

            $ionicLoading.show({template:'<p>Please wait.</p><ion-spinner></ion-spinner>'});

            groupServices.createGroup($scope.newGroupData)
                .then(function (result) {

                    $scope.closeModal();

                    //Insert New Data
                    $scope.groups.push(result.data);
                    $scope.newGroupData = {};
                    // Reload new group template
                    $ionicModal.fromTemplateUrl('modules/groups/views/newGroup.html', {scope: $scope})
                        .then(function (modal) {
                            $scope.newGroupModal = modal;
                        });
                    $ionicLoading.hide();
                })
                .catch(function (err) {
                    errorServices.logError(err, "trying to create group");
                });
        };

        $scope.doEditGroup = function () {

            if (typeof $scope.editGroupData.name == 'undefined' || $scope.editGroupData.name == ''){
                $ionicLoading.show({template: 'You cant edit a group with an empty name', duration:2000});
                return;
            }

            if (typeof $scope.editGroupData.level.code == 'undefined'){
                $ionicLoading.show({template: 'You cant edit a group without a level', duration:2000});
                return;
            }

            groupServices.updateGroup($scope.editGroupData)
                .then(function (result) {
                    $scope.closeModal();
                    $scope.editGroupData = {};
                    $scope.groups = [];
                    populateGroups();
                })
                .catch(function (err) {
                    errorServices.logError(err, "trying to edit group");
            });
        };

        $scope.removeGroup = function (group) {

            $translate(["titles.confirm_delete", "messages.confirm_delete", "buttons.yes"])
                .then(function(translations) {

                    var confirmPopup = $ionicPopup.confirm({
                        title: translations["titles.confirm_delete"],
                        template: translations["messages.confirm_delete"]
                    });

                    confirmPopup.then(function (res) {
                        if (res) {

                            $ionicLoading.show({template:'<p>Please wait.</p><ion-spinner></ion-spinner>'});

                            groupServices.removeGroup(group._id)

                                .then(function (result) {

                                    //REMOVE GHOST DATA
                                    var index = $scope.groups.map(function (g) {
                                        return g._id
                                    }).indexOf(group._id);
                                    $scope.groups.splice(index, 1);
                                    $ionicLoading.hide();
                                })

                        }
                    })
                })
                .catch(function (err) {
                    errorServices.logError(err, "trying to remove group");
                });
        };


        function loadGroupLevels() {

            groupServices.getGroupLevels(authenticationServices.userProfile.customer)

                .then(function (levels) {
                    $scope.groupLevels = levels.data;
                })

                .catch(function (err) {
                    errorServices.logError(err, "trying to fetch group levels");
                });
        }


        // LOCAL FUNCTIONS
        function populateGroups() {

            $ionicLoading.show({template:'<p>Fetching groups, please wait.</p><ion-spinner></ion-spinner>'});

            groupServices.getTeacherGroups()

                .then(function (groups) {
                    $ionicLoading.hide();
                    $scope.groups = groups.data;
                    $scope.loadingGroups = false;
                    $scope.$broadcast('scroll.refreshComplete'); // Stop the ion-refresher from spinning
                })

                .catch(function (err) {
                    $ionicLoading.hide();
                    $ionicLoading.show({template: 'We are embarrassed, there was an error loading groups. Please contact support', duration:2000});
                    errorServices.logError(err, "trying to populate groups");
                });
        }

    })


