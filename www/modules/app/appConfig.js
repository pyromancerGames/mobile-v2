(function () {
    angular.module('griffinmobile')

        .config(function ($stateProvider, $urlRouterProvider, $httpProvider, $momentProvider, jwtInterceptorProvider,
                          $translateProvider, $ionicConfigProvider) {
            $stateProvider
                .state('app', {
                    url: '/app',
                    abstract: true,
                    templateUrl: 'modules/core/views/menu.html',
                    controller: 'AppController'
                })

                .state('app.home', {
                    url: '/home',
                    views: {
                        'menuContent': {
                            templateUrl: 'modules/core/views/home.html'
                        }
                    }
                })

                .state('app.wall', {
                    url: '/wall',
                    views: {
                        'menuContent': {
                            templateUrl: 'modules/wall/views/newsWall.html',
                            controller: 'WallController'
                        }
                    }
                })

                .state('app.settings', {
                    url: '/settings',
                    views: {
                        'menuContent': {
                            templateUrl: 'modules/settings/views/settings.html',
                            controller: 'SettingsController'
                        }
                    }
                })

                .state('app.gamePulse', {
                    url: '/games_pulse',
                    views: {
                        'menuContent': {
                            templateUrl: 'modules/games/views/pulseGameMaster.html',
                            controller: 'PulseGameController'
                        }
                    }
                })

                .state('app.profile', {
                    url: '/profile/:userId',
                    views: {
                        'menuContent': {
                            templateUrl: 'modules/users/views/profile.html',
                            controller: 'ProfileController'
                        }
                    }
                })

                .state('app.broadcast', {
                    url: '/broadcast',
                    views: {
                        'menuContent': {
                            templateUrl: 'modules/broadcast/views/broadcast.html',
                            controller: 'BroadcastController'
                        }
                    }
                })

                .state('app.liveFeed', {
                    url: '/liveFeed',
                    views: {
                        'menuContent': {
                            templateUrl: 'modules/liveFeed/views/liveFeed.html',
                            controller: 'LiveFeedController'
                        }
                    }
                })

                .state('app.quests', {
                    url: '/quests',
                    views: {
                        'menuContent': {
                            templateUrl: 'modules/quests/views/quests.html',
                            controller: 'QuestsController'
                        }
                    }
                })

                .state('app.questDetail', {
                    url: '/quests/:questId',
                    views: {
                        'menuContent': {
                            templateUrl: 'modules/quests/views/questDetail.html',
                            controller: 'QuestDetailController'
                        }
                    }
                })

                .state('app.achievements', {
                    url: '/achievements',
                    views: {
                        'menuContent': {
                            templateUrl: 'modules/achievements/views/achievements.html',
                            controller: 'AchievementController'
                        }
                    }
                })

                .state('app.market', {
                    url: '/market',
                    views: {
                        'menuContent': {
                            templateUrl: 'modules/market/views/market.html'
                        }
                    }
                })

                .state('app.children', {
                    url: '/children',
                    views: {
                        'menuContent': {
                            templateUrl: 'modules/parents/views/children.html',
                            controller: 'ParentController'
                        }
                    }
                })

                .state('app.childrenStats', {
                    url: '/childrenStats/:userId',
                    views: {
                        'menuContent': {
                            templateUrl: 'modules/parents/views/childrenStats.html',
                            controller: 'ChildrenStatController'
                        }
                    }
                })

                .state('app.leaderboards', {
                    url: '/leaderboards',
                    views: {
                        'menuContent': {
                            templateUrl: 'modules/leaderboards/views/leaderboards.html',
                            controller: 'LeaderboardsController'
                        }
                    }
                })

                .state('app.students', {
                    url: '/students',
                    views: {
                        'menuContent': {
                            templateUrl: 'modules/users/views/teacherStudents.html',
                            controller: 'StudentsController'
                        }
                    }
                })

                .state('app.groups', {
                    url: '/groups',
                    views: {
                        'menuContent': {
                            templateUrl: 'modules/groups/views/groups.html',
                            controller: 'GroupsController'
                        }
                    }
                })

                .state('app.groupStudents', {
                    url: '/groups/:groupId',
                    views: {
                        'menuContent': {
                            templateUrl: 'modules/groups/views/groupStudents.html',
                            controller: 'GroupStudentsController'
                        }
                    }
                })

                .state('app.notifications', {
                    url: '/notifications',
                    views: {
                        'menuContent': {
                            templateUrl: 'modules/notifications/views/notifications.html',
                            controller: 'NotificationController'
                        }
                    }
                })

                .state('app.messages', {
                    url: '/messages',
                    views: {
                        'menuContent': {
                            templateUrl: 'modules/messages/views/messages.html',
                            controller: 'MessagesController'
                        }
                    }
                })

                .state('app.messagesDetail', {
                    url: '/messages/:user1/:user2',
                    views: {
                        'menuContent': {
                            templateUrl: 'modules/messages/views/messagesDetail.html',
                            controller: 'MessagesDetailController'
                        }
                    }
                })

                .state('app.store', {
                    url: '/store',
                    views: {
                        'menuContent': {
                            templateUrl: 'modules/store/views/store.html',
                            controller: 'StoreController'

                        }
                    }
                });

            // if none of the above states are matched, use this as the fallback
            $urlRouterProvider.otherwise('/app/home');

            //MOMENT JS
            $momentProvider
                .asyncLoading(false)
                .scriptUrl('//cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment.min.js');


            //JWT -- Interceptor to add token to requests
            jwtInterceptorProvider.tokenGetter = ['authenticationServices', 'config', function (authenticationServices, config) {

                // Skip authentication for any requests ending in .html
                if (config.url.substr(config.url.length - 5) == '.html') {
                    return null;
                }

                // User is logged in
                if (!authenticationServices.userIsLoggedIn()) {
                    return null;
                }

                // Inject token
                return authenticationServices.getToken();
            }];

            $httpProvider.interceptors.push('jwtInterceptor');
            $httpProvider.defaults.timeout = 5000;

            // i18n
            $translateProvider.useStaticFilesLoader({
                prefix:'i18n/locale-',
                suffix:'.json'
            });

            $translateProvider.useSanitizeValueStrategy('escape');
            //$translateProvider.preferredLanguage('es');
            $translateProvider.preferredLanguage('en');
            $translateProvider.fallbackLanguage('en');
            $ionicConfigProvider.tabs.position("bottom");
            $ionicConfigProvider.navBar.alignTitle('left')

        })

})();

