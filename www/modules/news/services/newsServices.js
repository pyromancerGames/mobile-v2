(function () {
    angular.module('classbuzz.news')
        .factory('newsServices', function ($rootScope, $localStorage, $location, _, apiServices,
                                           authenticationServices) {
            var services = {

                /**
                 * Get news for a user, lazy loading
                 * @returns {*}
                 */
                getLazyNews: function (from) {

                    var userId = authenticationServices.userProfile.id;
                    var _data = {

                        fromIndex: from,
                        limit: 6,
                        userId: userId
                    };

                    var endpoint = '/news/lazy';
                    return apiServices.POST(endpoint, _data);
                },


                createNews: function (_data) {

                    if (typeof _data.customer == "undefined") _data.customer = authenticationServices.userProfile.customer;
                    if (typeof _data.author == "undefined") _data.author = authenticationServices.userProfile.id;

                    var endpoint = '/news';
                    return apiServices.POST(endpoint, _data);
                },


                removePost: function (post) {
                    var endpoint = '/news/' + post._id;
                    return apiServices.DELETE(endpoint);
                },

                addComment: function(newsId, text) {

                    var _data = {};
                    _data.text = text;

                    var endpoint = '/news/' + newsId + '/comment';
                    return apiServices.POST(endpoint, _data);
                },


                addReaction: function(newsId, type) {

                    if (typeof type == "undefined") type = 'like';

                    var _data = {};
                    _data.type = type;

                    var endpoint = '/news/' + newsId + '/reaction';
                    return apiServices.POST(endpoint, _data);
                },


                removeComment: function(newsId, commentId){

                    var endpoint = '/news/' + newsId + '/comment/' + commentId;
                    return apiServices.DELETE(endpoint);

                },

                removeReaction: function(newsId, reactionId){

                    var endpoint = '/news/' + newsId + '/reaction/' + reactionId;
                    return apiServices.DELETE(endpoint);

                }
            };

            return services;
        });

})();





