(function () {

    angular.module('classbuzz.notifications')

        .factory('notificationServices', function ($http, $rootScope, authenticationServices, apiServices) {

            var notificationServices = {

                getPendingNotifications: function (userId) {
                    if (typeof userId == "undefined" || userId == null) userId = authenticationServices.userProfile.id;
                    var _endpoint = '/notifications/user/' + userId;
                    return apiServices.GET(_endpoint);
                },


                deleteNotification: function (notificationId) {
                    var _endpoint = '/notifications/remove';
                    var _data = {_id: notificationId};
                    return apiServices.POST(_endpoint, _data);
                },

                markAsRead: function (notificationId) {

                    var _endpoint = '/notifications/read';
                    var _data = {_id: notificationId};
                    return apiServices.POST(_endpoint, _data);
                },


                markAllAsRead: function (userId) {

                    if (typeof userId == "undefined" || userId == null) userId = authenticationServices.userProfile.id;

                    var _endpoint = '/notifications/user/'+userId+'/read/all';
                    return apiServices.POST(_endpoint, {});
                },


                /**
                 * Get notifications for a user, lazy loading
                 * @returns {*}
                 */
                getLazyNotifications: function (from, userId) {

                    if (typeof userId == "undefined" || userId == null) userId = authenticationServices.userProfile.id;

                    var _data = {
                        fromIndex: from,
                        limit: 6
                    };

                    var endpoint = '/notifications/user/'+userId+'/lazy';
                    return apiServices.POST(endpoint, _data);

                }

            };

            return notificationServices;
        });

})();
