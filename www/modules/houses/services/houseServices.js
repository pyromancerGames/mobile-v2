(function () {
    angular.module('classbuzz.houses')
        .factory('houseServices', function ($rootScope, $localStorage, $location, _,
                                            apiServices, authenticationServices) {
            var services = {

                /**
                 * Get houses for a customer
                 * @param [customerId] - defaults to logged user customer
                 * @returns {*}
                 */
                getHouses: function (customerId) {

                    if (typeof customerId == "undefined" || customerId == null) customerId = authenticationServices.userProfile.customer;
                    var endpoint = '/houses/customer/' + customerId;
                    return apiServices.GET(endpoint);
                }

            };
            return services;
        });

})();



